//Use the required plugins
var gulp = require('gulp');
var browserSync = require('browser-sync');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var del = require('del');
var runSequence = require('run-sequence');
var ngAnnotate = require('gulp-ng-annotate');
var plumber = require('gulp-plumber');
var replace = require('gulp-replace');
var proxyMiddleware = require('http-proxy-middleware');
var war = require('gulp-war');
var zip = require('gulp-zip');
var argv = require('yargs').argv;

// Required localization task
var xlsx = require('node-xlsx');
var jf = require('jsonfile');
var lodash = require('lodash');
var jbuilder = require('jbuilder');
var fs = require('fs');
var es = require('event-stream');
var vinylBuffer = require('vinyl-buffer');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');

// Angular template cache
var templateCache = require('gulp-angular-templatecache');

// Angular file sort
var angularFilesort = require('gulp-angular-filesort');

// Concatenate the templates with main.min.js
var concat = require('gulp-concat');

// Localization task
function appendToMergedJson(mergedJson, fileToRead){
    var excelContent = xlsx.parse(fs.readFileSync(fileToRead));

    var excelJson = jbuilder.encode(function (json) {
        var rowLength = excelContent[0].data.length;
        var colFieldsLength = excelContent[0].data[0].length;
        for (var k = 0; k < colFieldsLength; k++) {
            // Set second level values
            json.set(excelContent[0].data[0][k], function (json) {
                for (var row = 1; row < rowLength; row++) {
                    json.set(excelContent[0].data[row][0], excelContent[0].data[row][k]);
                }
            });
        }
    });
    excelJson = JSON.parse(excelJson);
    return lodash.merge({},mergedJson, excelJson);
}
gulp.task('genLocalizations', function (cb) {
    jf.spaces = 4;
    var omitList = ['LANGUAGE-KEY', 'Component'];
    var mergedJson = {};
    var fileToRead = 'localization.xlsx';
    mergedJson = appendToMergedJson(mergedJson, fileToRead);
    // we'll keep our assets in memory
    var memory = {};
    // we make an array to store all the stream promises
    var streams = [];
    // we store all the different locale file names in an array
    var availableLocaleFiles = [];
    for (var prop in mergedJson) {
        if (mergedJson.hasOwnProperty(prop)) {
            if (omitList.indexOf(prop) > -1) continue;
            availableLocaleFiles.push(prop.toLowerCase());
            memory[prop.toLowerCase()] = JSON.stringify(mergedJson[prop]);
        }
    }
    availableLocaleFiles.forEach(function (v) {
        // make a new stream with fake file name
        var stream = source(v + '.json');
        var fileContents = memory[v];
        streams.push(stream);
        // write the file contents to the stream
        stream.write(fileContents);
        process.nextTick(function () {
            // in the next process cycle, end the stream
            stream.end();
        });
        stream
        // transform the raw data into the stream, into a vinyl object/file
            .pipe(vinylBuffer())
            .pipe(gulp.dest('app/locale/'));
    });
    return es.merge.apply(this, streams);
});


gulp.task('templateCache', function () {
    return gulp.src('app/js/**/*.html')
        .pipe(templateCache('templates.js',{
            module: "blackmap",
            transformUrl: function(url) {
                return "js/" + url;
            }
        }))
        .pipe(gulp.dest('app/js/blackmap'));
});

gulp.task('copyFonts', function () {
    return gulp
        .src("app/css/bootstrap/fonts/**")
        .pipe(gulp.dest("dist/fonts/"));
});

//For server hosting use BrowserSync
gulp.task('browserSync', function () {
    browserSync.init({
        notify: false,
        server: {
            baseDir: 'app',
            middleware: {
                1: proxyMiddleware(function (path) {
                    return /geoserver/.test(path)
                }, {
                    target: "http://h2729929.stratoserver.net:8080",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                }),
                2: proxyMiddleware(function (path) {
                    return /losm/.test(path) || /mapurl/.test(path)
                }, {
                    target: "http://h2729929.stratoserver.net:8080",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                })
            }
        }
    });
});


//For server hosting use BrowserSync
gulp.task('browserSync:sd', function () {
    browserSync.init({
        notify: false,
        server: {
            baseDir: 'app',
            middleware: {
                1: proxyMiddleware(function (path) {
                    return /geoserver/.test(path)
                }, {
                    target: "http://localhost:10080/",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                }),
                2: proxyMiddleware(function (path) {
                    return /losm/.test(path) || /mapurl/.test(path)
                }, {
                    target: "http://localhost:10080/",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                })
            }
        }
    });
});

//For server hosting use BrowserSync
gulp.task('browserSync:dist', function () {
    browserSync.init({
        notify: false,
        server: {
            baseDir: 'dist',
            middleware: {
                1: proxyMiddleware(function (path) {
                    return /geoserver/.test(path)
                }, {
                    target: "http://h2729929.stratoserver.net:8080",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                }),
                2: proxyMiddleware(function (path) {
                    return /losm/.test(path) || /mapurl/.test(path)
                }, {
                    target: "http://h2729929.stratoserver.net:8080",
                    protocolRewrite: 'http',
                    changeOrigin: true   // for vhosted sites, changes host header to match to target's host
                })
            }
        }
    });
});

/****************************************

 Use this command "gulp serve" to host the app on local server.
 In this task, gulp watches the specified files for changes and reloads the browser on every change made.

 ****************************************/

gulp.task('serve', ['templateCache', 'browserSync'], function () {
    // Reloads the browser whenever HTML or JS or CSS files change and Uses browserSync.reload to reload the browser
    gulp.watch('app/css/*.css', browserSync.reload);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);

});


gulp.task('serve:sd', ['templateCache', 'browserSync:sd'], function () {
    // Reloads the browser whenever HTML or JS or CSS files change and Uses browserSync.reload to reload the browser
    gulp.watch('app/css/*.css', browserSync.reload);
    gulp.watch('app/**/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);

});

gulp.task('serve:dist', ['templateCache', 'browserSync:dist'], function () {
    // Reloads the browser whenever HTML or JS or CSS files change and Uses browserSync.reload to reload the browser
    gulp.watch('dist/css/*.css', browserSync.reload);
    gulp.watch('dist/**/*.html', browserSync.reload);
    gulp.watch('dist/js/**/*.js', browserSync.reload);

});




/****************************************

 The tasks given below are for distribution.
 These tasks are used to create a dist folder for distribution purpose in which files are minified and could be pushed for publishing.
 The command for this is "gulp build".

 ****************************************/

//This is a sub-task for minifying js files
//In index.html it changes all the script tags within "<!--build:" and "<!--endbuild-->" into one single JavaScript file that points to `js/main.min.js`(the file name specified).
//Check index.html for sample
//Have a look at the documentation of useref for more information

gulp.task('copytoserver', function () {
    gulp.src('./dist/**/*')
        .pipe(gulp.dest('../liveosm/src/main/resources/static/public'));
});

gulp.task('useref', function () {
    return gulp.src('app/*.html')
        .pipe(useref())

        //.pipe(plumber())
        .pipe(gulpIf('app/js/**/*.js', angularFilesort()))
        //Uses annotation for possible injection errors
        .pipe(gulpIf('app/js/**/*.js', ngAnnotate()))
        // Minifies only if it's a JavaScript file
        .pipe(gulpIf('app/**/*.css', cssnano()))
        .pipe(gulp.dest('dist'))
    //.pipe(gulp.dest('../liveosm/src/main/resources/static/public'))

});

gulp.task('uglifyJs', function() {
    return gulp.src('dist/js/main.min.js')
        .pipe(uglify({
            mangle: true,
            outSourceMap: true,
            compress: {
                drop_console: true
            },
            preserveComments: 'license'
        }).on('error', gutil.log))
        .pipe(gulp.dest('dist/js'));


});

gulp.task('concatTemplates', function() {
    return gulp.src(['dist/js/main.min.js', 'app/js/blackmap/templates.js'])
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('dist/js'));
});

//This sub-task cleans the distribution folder everytime before "gulp build is used"
gulp.task('clean:dist', function () {
    return del.sync('dist/*');
});

//This sub-task pipes the images to dist folder
gulp.task('images', function () {
    return gulp.src('app/img/**/*')
        .pipe(gulp.dest('dist/img'));
});

//This sub-task pipes all locale to the dist folder
gulp.task('locale', function () {
    return gulp.src('app/locale/*')
        .pipe(gulp.dest('dist/locale'))
});

gulp.task('war', ['build'], function () {
    del.sync('war/*');
    gulp.src("dist/**/*")
        .pipe(war({
            welcome: 'index.html',
            displayName: 'losma'
        }))
        .pipe(zip('losma.war'))
        .pipe(gulp.dest("war/"));

});


/****************************************

 This is the main task, which should be used as "gulp build" to push the app to distribution
 It uses runSequence to run all the gulp sub-tasks in sequence.

 ****************************************/

gulp.task('build', function (callback) {
    runSequence('clean:dist', ['copyFonts', 'templateCache', 'useref', 'locale','images'], 'concatTemplates', 'uglifyJs',
        callback
    );
});
