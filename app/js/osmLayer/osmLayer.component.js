/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.osmLayer')
    .component('osmLayer', {
        templateUrl: 'js/osmLayer/osmLayer.html',
        controller: OsmLayerController,
        bindings: {
            styleClass: '@',
            map: '='
        }
    });

OsmLayerController.$inject = ['osmLayer'];
function OsmLayerController(osmLayer) {
    var ctrl = this;
    ctrl.layerVisibility = true;
    ctrl.turnLayerVisibility = turnLayerVisibility;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    function turnLayerVisibility() {
        ctrl.layerVisibility = !ctrl.layerVisibility;
        osmLayer.setLayerVisibility(ctrl.map, ctrl.layerVisibility);
    }
}