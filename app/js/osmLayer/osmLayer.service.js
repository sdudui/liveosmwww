/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.osmLayer')
    .service('osmLayer', osmLayer);

osmLayer.$inject = [];
function osmLayer() {
    var osmLayerAPI = {
        layerName: 'osm',
        setLayerVisibility: setLayerVisibility
    };

    return osmLayerAPI;

    function setLayerVisibility(map, isVisible) {
        map.getLayers().forEach(function (layer) {
            if (osmLayerAPI.layerName.includes(layer.get('name'))) {
                layer.setVisible(isVisible);
            }
        });
    }
}