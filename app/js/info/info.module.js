/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.info', [
    'ngAnimate',
    'ngSanitize',

    // Third party angular components
    'ui.router',
    'ui.bootstrap.modal'
]);