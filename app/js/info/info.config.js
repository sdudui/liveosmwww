/**
 * Created by sm on 13.05.2017.
 */
(function(){
    angular.module('blackmap.info')
        .config(configure);
    configure.$inject = ['$stateProvider'];
    function configure($stateProvider) {

        // Routes config
        var states = [
            {
                name: 'blackmap.info',
                url: '/info',
                component: 'blackmapInfo'
            }
        ];
        // Loop over the state definitions and register them
        states.forEach(function(state) {
            $stateProvider.state(state);
        });
    }
}());
