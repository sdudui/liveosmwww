/**
 * Created by sm on 06.05.2017.
 */
angular
    .module('blackmap.info')
    .component('blackmapInfo', {
        templateUrl: 'js/info/info.html',
        controller: InfoController,
        bindings: {}
    });

InfoController.$inject = ['$scope', '$uibModal'];
function InfoController($scope, $uibModal) {
    var modalInstance;
    var ctrl = this;
    ctrl.open = open;
    ctrl.close = close;

    ctrl.$onDestroy = function (event) {
        modalInstance = null;
    };

    ctrl.$onInit = function () {
        ctrl.open();
    };

    function open() {
        modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'infoModal.html',
            scope: $scope,
            size: "md"
        });
    }

    function close() {
        modalInstance.close();
    }
}