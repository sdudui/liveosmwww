/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.zoomInfo')
    .component('zoomInfo', {
        templateUrl: 'js/zoomInfo/zoomInfo.html',
        controller: ZoomInfoController,
        bindings: {
            map: '<'
        }
    });
ZoomInfoController.$inject = ['$interval'];
function ZoomInfoController($interval){
    var ctrl = this;
    var isZoomInitialized = false;
    ctrl.zoom = 0;

    ctrl.$onDestroy = function (event) {
        ctrl.map.un("moveend", onMapMoveend);
    };

    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isZoomInitialized){
            ctrl.map.on("moveend", onMapMoveend);
            isZoomInitialized = true;
        }
    };

    function onMapMoveend(event){
        $interval(function(){
            ctrl.zoom = parseInt(ctrl.map.getView().getZoom());
        },1,1);
    }
}