/**
 * Created by sm on 07.05.2017.
 */

angular.module('blackmap.userBuildingLayer')
    .service('userBuildingLayer', userBuildingLayer);

userBuildingLayer.$inject = [];
function userBuildingLayer() {
    var userBuildingLayerAPI = {
        layerName: 'userBuilding',
        layer: null,
        getLayer: getLayer,
        addUserBuildingLayer: addUserBuildingLayer,
        setLayerVisibility: setLayerVisibility
    };

    return userBuildingLayerAPI;

    function getLayer(){
        return userBuildingLayerAPI.layer;
    }

    function addUserBuildingLayer(map){
        userBuildingLayerAPI.layer = new ol.layer.Vector({
            name: userBuildingLayerAPI.layerName,
            source: new ol.source.Vector(),
            style: new ol.style.Style({
                fill: new ol.style.Fill({
                    color: 'rgba(0, 0, 0, 0.5)'
                }),
                stroke: new ol.style.Stroke({
                    color: 'black',
                    width: 2
                })
            })
        });
        map.addLayer(userBuildingLayerAPI.layer);
    }

    function setLayerVisibility(map, isVisible) {
        map.getLayers().forEach(function (layer) {
            if (userBuildingLayerAPI.layerName.includes(layer.get('name'))) {
                layer.setVisible(isVisible);
            }
        });
    }
}