/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.userBuildingLayer')
    .component('userBuildingLayer', {
        templateUrl: 'js/userBuildingLayer/userBuildingLayer.html',
        controller: UserBuildingLayerController,
        bindings: {
            map: '<',
            userBuildingLayer: '=',
            isVisible: '<'
        }
    });

UserBuildingLayerController.$inject = ['userBuildingLayer'];
function UserBuildingLayerController(userBuildingLayer) {
    var ctrl = this;
    var isLayerInitialized = false;
    var layerName = ctrl.layerName;
    ctrl.layerVisibility = true;
    ctrl.turnLayerVisibility = turnLayerVisibility;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isLayerInitialized){
            isLayerInitialized = true;
            userBuildingLayer.addUserBuildingLayer(ctrl.map);
            ctrl.userBuildingLayer = userBuildingLayer.getLayer();
            if(_.isUndefined(ctrl.isVisible)) ctrl.isVisible = true;
        }
    };

    function turnLayerVisibility() {
        ctrl.layerVisibility = !ctrl.layerVisibility;
        userBuildingLayer.setLayerVisibility(ctrl.map, ctrl.layerVisibility);
    }
}