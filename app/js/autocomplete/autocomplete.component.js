/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.autocomplete')
    .component('autocomplete', {
        templateUrl: 'js/autocomplete/autocomplete.html',
        controller: AutocompleteController,
        bindings: {
            map: '<',
            addressLocation: '<',
            onMenuToggled: '&',
            onAddressChanged: '&'
        }
    });
AutocompleteController.$inject = ['$interval', 'autocomplete'];
function AutocompleteController($interval, autocomplete) {
    var ctrl = this;
    ctrl.location = "";
    ctrl.modelOptions = {
        debounce: {
            default: 100,
            blur: 250
        },
        getterSetter: false
    };
    ctrl.onToggleMenu = onToggleMenu;
    ctrl.fetchLocations = fetchLocations;
    ctrl.showOnMap = showOnMap;
    ctrl.clearAddress = clearAddress;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    function onToggleMenu(){
        ctrl.onMenuToggled()();
    }

    function fetchLocations(location){
        return autocomplete.fetchLocations(location);
    }

    function showOnMap($item, $model, $label, $event){
        ctrl.location = $item.name;
        $interval(function(){
            ctrl.addressLocation = $item.id;
            var coords = ol.proj.transform([ctrl.addressLocation.lng, ctrl.addressLocation.lat], "EPSG:4326", "EPSG:3857");
            ctrl.onAddressChanged()(coords, true, 15);
        }, 1, 1);

    }

    function clearAddress(){
        ctrl.location = '';
        ctrl.onAddressChanged()();
    }
}