/**
 * Created by sm on 14.05.2017.
 */

angular
    .module('blackmap.userBuildingDownload')
    .component('userBuildingDownload', {
        templateUrl: 'js/userBuildingDownload/userBuildingDownload.html',
        controller: UserBuildingDownloadController,
        bindings: {
            userBuildingLayer: '<'
        }
    });
UserBuildingDownloadController.$inject = ['FileSaver', 'Blob'];
function UserBuildingDownloadController(FileSaver, Blob) {
    var ctrl = this;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    ctrl.download = function(){
        var features = (new ol.format.GeoJSON()).writeFeatures(ctrl.userBuildingLayer.getSource().getFeatures(), {featureProjection: 'EPSG:3857'});
        var data = new Blob([features], { type: 'application/json;charset=utf-8' });
        FileSaver.saveAs(data, 'schwarzplan.json');
    };
}