/**
 * Created by sm on 06.05.2017.
 */



angular
    .module('blackmap')
    .component('blackmap', {
        templateUrl: 'js/blackmap/blackmap.html',
        controller: BlackmapController,
        bindings: {
            startLocation: '<?',
            isAppStartDoGeolocation: '<?'
        }
    });

BlackmapController.$inject = ['$interval', '$window'];
function BlackmapController($interval, $window) {
    var currentLocationOverlay = null;
    var currentLocationElm = document.createElement("span");
    currentLocationElm.setAttribute('class', 'glyphicon glyphicon-map-marker current-location-overlay');
    var ctrl = this;
    var isBlackmapInitialized = false;
    ctrl.dragBuilding = false;
    ctrl.dragBuildingTopLeftPixel = [0, 0];
    ctrl.buildingLayer = null;
    ctrl.userBuildingLayer = null;
    ctrl.wrkGridLayer = null;
    ctrl.selectedBuildingFeatures = [];
    ctrl.map = null;
    ctrl.geoserver = null;
    ctrl.wrkMap = null;
    ctrl.isToolOpen = true;
    ctrl.isToolShow = $window.innerWidth > 400; // Check for smaller device width and initialize blackmap tools visibility to false.
    ctrl.updateCurrentLocationOverlay = updateCurrentLocationOverlay;
    ctrl.dragEndBuilding = dragEndBuilding;
    ctrl.toggleMenu = toggleMenu;
    ctrl.toggleOpen = toggleOpen;

    ctrl.$onDestroy = function (event) {};

    ctrl.$onInit = function () {};

    ctrl.$doCheck = function(){
        if(ctrl.startLocation && ctrl.map && !isBlackmapInitialized){
            var coords = ol.proj.transform([ctrl.startLocation.id.lng, ctrl.startLocation.id.lat], "EPSG:4326", "EPSG:3857");
            ctrl.map.getView().setCenter(coords);
            isBlackmapInitialized = true;
        }
    };

    function updateCurrentLocationOverlay(position, isSetCenter, zoom){
        if(!_.isNull(currentLocationOverlay)) ctrl.map.removeOverlay(currentLocationOverlay);
        if(position){
            currentLocationOverlay =  new ol.Overlay({
                offset: [0, 0],
                positioning: 'center-center',
                element: currentLocationElm,
                stopEvent: false
            });
            currentLocationOverlay.setPosition(position);
            ctrl.map.addOverlay(currentLocationOverlay);
            if(isSetCenter) ctrl.map.getView().setCenter(position);
            if(zoom) ctrl.map.getView().setZoom(zoom);
        }
    }

    function dragEndBuilding(event){
        var format = new ol.format.GeoJSON();
        var olFeature = format.readFeature(JSON.stringify(ctrl.dragBuilding.featureJson), {featureProjection: 'EPSG:3857'});
        var overlayFeature = olFeature.clone();
        ctrl.userBuildingLayer.getSource().addFeature(overlayFeature);
        ctrl.dragBuilding = false;
        ctrl.dragBuildingTopLeftPixel = [0, 0];
        var buildingCoordinate = ctrl.map.getCoordinateFromPixel([event.clientX, event.clientY]);
        var geometry = overlayFeature.getGeometry();
        var olFeatureCoordinate = geometry.getLastCoordinate();
        var deltaX = buildingCoordinate[0] - olFeatureCoordinate[0];
        var deltaY = buildingCoordinate[1] - olFeatureCoordinate[1];
        geometry.translate(deltaX, deltaY);
        overlayFeature.setGeometry(geometry);
    }

    function toggleMenu(isShow){
        ctrl.isToolShow = _.isUndefined(isShow) ? !ctrl.isToolShow : isShow;
        ctrl.isToolOpen = ctrl.isToolShow;
        if(!ctrl.isToolShow){
            resetDragBuilding();
        }
    }

    function toggleOpen(){
        if(!ctrl.isToolOpen){
            resetDragBuilding();
        }
    }

    function resetDragBuilding(){
        $interval(function(){
            ctrl.dragBuilding = false;
            ctrl.dragBuildingTopLeftPixel = [0, 0];
        }, 1, 1);
    }
}
