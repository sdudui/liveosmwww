(function(){
    angular.module('blackmap')
        .config(configure);
    configure.$inject = ['$windowProvider', '$locationProvider', '$urlRouterProvider', '$stateProvider', '$compileProvider', '$logProvider', 'cfpLoadingBarProvider', '$translateProvider', 'toastrConfig'];
    function configure($windowProvider, $locationProvider, $urlRouterProvider, $stateProvider, $compileProvider, $logProvider, cfpLoadingBarProvider, $translateProvider, toastrConfig) {
        // HTML5 mode settings
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false,
            rewriteLinks: true
        });
        $locationProvider.hashPrefix('');

        $urlRouterProvider
            .when('', '/blackmap')
            .when('/', '/blackmap')
            .when('/blackmap/', ['$state', function ($state) {
                $state.go("blackmap");
            }]);


        // Routes config
        var states = [
            {
                name: 'blackmap',
                url: '/blackmap',
                component: 'blackmap',
                resolve: {
                    blackmap: ['olmap', function(map){
                        return map.getUrls(ajaxServer);
                    }],
                    isAppStartDoGeolocation: ['blackmap', '$stateParams', function(blackmap, $stateParams){
                        return _.isEmpty($stateParams.location);
                    }]
                }
            },
            {
                name: 'blackmap.location',
                url: '/{location}',
                component: 'blackmap',
                resolve: {
                    blackmap: ['olmap', function(map){
                        return map.getUrls(ajaxServer);
                    }],
                    isAppStartDoGeolocation: ['blackmap', '$stateParams', function(blackmap, $stateParams){
                        return _.isEmpty($stateParams.location);
                    }],
                    startLocation: ['$stateParams', 'autocomplete', function($stateParams, autocomplete){
                        return autocomplete.fetchLocations($stateParams.location).then(function(response){
                            var result = false;
                            if(_.isUndefined(response)) return result;
                            if(_.isUndefined(response[0])) return result;
                            return response[0];
                        });
                    }]
                }
            }
        ];

        // Loop over the state definitions and register them
        states.forEach(function(state) {
            $stateProvider.state(state);
        });

        // Angular performance option
        $compileProvider.debugInfoEnabled(false);
        $compileProvider.preAssignBindingsEnabled(true);

        // configure how the application logs messages
        $logProvider.debugEnabled(false);

        // Angular loading bar spinner config
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 200;

        // Localization config
        $translateProvider.useStaticFilesLoader({
            prefix: 'locale/',
            suffix: '.json'
        });
        var $window = $windowProvider.$get();
        var lang = $window.navigator.language || $window.navigator.userLanguage;
        $translateProvider.preferredLanguage(lang.split('-')[0]);
        $translateProvider.fallbackLanguage('en');
        $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

        // Toastr notifications configuration
        angular.extend(toastrConfig, {
            allowHtml: true,
            closeButton: true,
            closeHtml: '<button>&times;</button>',
            extendedTimeOut: 1000,
            autoDismiss: true,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: true,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
            preventOpenDuplicates: true,
            target: 'body',
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            messageClass: 'toast-message',
            onHidden: null,
            onShown: null,
            onTap: null,
            progressBar: false,
            tapToDismiss: true,
            /*templates: {
             toast: 'directives/toast/toast.html',
             progressbar: 'directives/progressbar/progressbar.html'
             },*/
            timeOut: 5000,
            titleClass: 'toast-title',
            toastClass: 'toast'
        });
    }
}());
