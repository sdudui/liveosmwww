angular.module('blackmap', [
    // Angular components
    'ngAnimate',
    'ngSanitize',
    'ngTouch',
    'ngLocale',

    // Third party angular components
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
    'pascalprecht.translate',
    'toastr',
    'ui.bootstrap.accordion',

    // Blackmap app components
    'blackmap.map',
    'blackmap.geolocation',
    'blackmap.info',
    'blackmap.zoomInfo',
    'blackmap.featureExport',
    'blackmap.build',
    'blackmap.gridLayer',
    'blackmap.buildingLayer',
    'blackmap.otherLayer',
    'blackmap.osmLayer',
    'blackmap.autocomplete',
    'blackmap.tools',
    'blackmap.userBuildingLayer',
    'blackmap.wrkGridLayer',
    'blackmap.buildingDrag',
    'blackmap.print',
    'angular-cookie-law'
]);
