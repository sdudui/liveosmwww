/**
 * Created by sm on 17.07.2017.
 */

(function(){
    'use strict';
    angular.module('blackmap').run(['$uiRouter', '$trace', function($uiRouter, $trace) {
        var Visualizer = window['ui-router-visualizer'].Visualizer;
        var pluginInstance = $uiRouter.plugin(Visualizer);
        $trace.enable('TRANSITION')
    }]);
}());