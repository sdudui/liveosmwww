/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.build')
    .component('buildBlackmap', {
        templateUrl: 'js/buildBlackmap/buildBlackmap.html',
        controller: BuildBlackmapController,
        bindings: {
            map: '<',
            wrkGridLayer: '='
        }
    });

BuildBlackmapController.$inject = ['$interval', 'buildBlackmap'];
function BuildBlackmapController($interval, buildBlackmap){
    var stop;
    var ctrl = this;
    ctrl.workStatus = {
        workTimeEllapsed: "--",
        statusConstruct: false,
        workStatusMessage: ""
    };


    ctrl.buildOptions = [
        {title:'blackmap_buildBlackmap_updt_button_title',
        option: 'delend'},
        {title:'blackmap_buildBlackmap_new_button_title' ,
            option: 'end'}
    ];



    ctrl.constructOSMFeatures = constructOSMFeatures;
    ctrl.refreshConstructStatus = refreshConstructStatus;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    function constructOSMFeatures(option){
        var i = 0;
        if (!ctrl.workStatus.statusConstruct) {
            ctrl.workStatus.statusConstruct = true;

            if(ctrl.map.getView().getZoom() < 17)
                buildBlackmap.zoomToLevel(ctrl.map, 17);



            ctrl.workStatus.workStatusMessage = "Please wait ...";
            ctrl.workStatus.workTimeEllapsed = 60;
            ctrl.wrkGridLayer.timeToNextCall = 0;
            buildBlackmap.constructWrkGridLayer(ctrl.map, ajaxServer,  option, ctrl.workStatus.extent, ctrl.wrkGridLayer);
            ctrl.wrkGridLayer.update = true;
            stop = $interval(function () {
                i++;
                buildBlackmap.testConstructFinished(ctrl.wrkGridLayer, ajaxServer, ctrl.refreshConstructStatus, i, ctrl.map);
            }, 3000);

        }
    }

    function refreshConstructStatus(result, i) {
        if (result == 0 || i > 20 || (60 - 3 * i < ctrl.wrkGridLayer.timeToNextCall)) {
            $interval.cancel(stop);
            ctrl.wrkGridLayer.update = false;
            ctrl.wrkGridLayer.getSource().clear();
            ctrl.workStatus.statusConstruct = false;
            ctrl.workStatus.workTimeEllapsed = "---";

            if (result > 0) {
                ctrl.workStatus.extent = null;
                ctrl.workStatus.workStatusMessage = "Done partially. Try again ...";

            }
            else {
                ctrl.workStatus.extent = null;
                ctrl.workStatus.workStatusMessage = "Done";

            }
            buildBlackmap.refreshWMS(ctrl.map);
        }
        else {
            ctrl.workStatus.workStatusMessage = "Please wait...";
            ctrl.workStatus.workTimeEllapsed = 60 - i * 3;
        }


    }

}