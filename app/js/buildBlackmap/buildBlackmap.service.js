/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.build')
    .service('buildBlackmap', buildBlackmap);

buildBlackmap.$inject = ["$http","$timeout"];
function buildBlackmap($http, $timeout) {
    var buildBlackmapAPI = {
        constructOSMFeature: constructOSMFeature,
        constructWrkGridLayer: constructWrkGridLayer,
        refreshWMS: refreshWMS,
        zoomToLevel: zoomToLevel,
        testConstructFinished:testConstructFinished

    };

    return buildBlackmapAPI;

    function testConstructFinished(wrkGridLayer, ajaxServer, cs, i, map){
        var m = map;
        i++;
        console.log(i);
        var j = 0;
        var gridsReady = [];
        wrkGridLayer.getSource().getFeatures().forEach(function(s){
            if(s.status == 'w')
                j++;
            else if(s.status == 't' && s.osmTileStub.osmElements) {
                s.status = 'p';
                s.osmTileStub.wkt = null;
                s.osmTileStub.query = null;
                gridsReady.push(s.osmTileStub);
            }

            }
        );
        console.log(gridsReady.length);
        if(gridsReady.length > 0){
            var url = "/grid/create";
            $timeout(function(){
                $http({
                    method: 'POST',
                    url: ajaxServer + url,
                    data: gridsReady,
                    responseType: 'json',
                    headers: {
                        "Content-Type": "application/json"
                    }
                }) .then(function successCallback(data1) {
                    $timeout(refreshWMS,500,false,map);

                }, function errorCallback(response1) {
                    console.log(response);
                });
            },1000,false);

        }

        cs(j,i);

    }

    function constructWrkGridLayer(map, ajaxServer,term, extent, wrkGridLayer){
        var zoom = map.getView().getZoom();
        if (zoom < 17 && term == 'end') {
            map.getView().setZoom(17);
        }
        function wrapLon(value) {
            var worlds = Math.floor((value + 180) / 360);
            return value - (worlds * 360);
        }
        if(term == 'end' || !extent)
            extent = map.getView().calculateExtent(map.getSize());
        var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent),
            'EPSG:3857', 'EPSG:4326');
        var topRight = ol.proj.transform(ol.extent.getTopRight(extent),
            'EPSG:3857', 'EPSG:4326');
        var url = "/freegrids" + "/" + wrapLon(bottomLeft[0]) + "/" + bottomLeft[1] + "/" + wrapLon(topRight[0]) + "/" + topRight[1] + "/" + term;

        var resp = [];
        $http({
            method: 'GET',
            url: ajaxServer + url
        }).then(function (response) {
            var data = response.data;
            if(data){
                wrkGridLayer.addFeatures(data.osmTileStub,$http, $timeout);
            }

        });

        return extent;
    }

    function constructOSMFeature(map, ajaxServer, cs, i, term, extent){
        var zoom = map.getView().getZoom();
        if (zoom < 17 && term == 'end') {
            map.getView().setZoom(17);
        }
        function wrapLon(value) {
            var worlds = Math.floor((value + 180) / 360);
            return value - (worlds * 360);
        }
        if(term == 'end' || !extent)
            extent = map.getView().calculateExtent(map.getSize());
        var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(extent),
            'EPSG:3857', 'EPSG:4326');
        var topRight = ol.proj.transform(ol.extent.getTopRight(extent),
            'EPSG:3857', 'EPSG:4326');
        var url = "/extent" + "/" + wrapLon(bottomLeft[0]) + "/" + bottomLeft[1] + "/" + wrapLon(topRight[0]) + "/" + topRight[1] + "/" + term;

        var resp = [];
        $http({
            method: 'GET',
            url: ajaxServer + url
        }).then(function (response) {
            var data = response.data;
            if(data){
                resp = parseInt(data.response);
                if(Object.prototype.toString.call(cs) == '[object Function]')
                    cs(resp, i);
            }

        });

        return extent;
    }

    function refreshWMS(map){
        map.getLayers().forEach(function (layer) {
            if (layer.get('name') == "tcache" || layer.get('name') == "mcache") {
                var params = layer.getSource().getParams();
                params.t = new Date().getMilliseconds();
                layer.getSource().updateParams(params);

            }
        });
        map.updateSize();
    }

    function zoomToLevel(map, level){
        map.getOverlays().clear();
        map.getView().setZoom(level);
    }


}