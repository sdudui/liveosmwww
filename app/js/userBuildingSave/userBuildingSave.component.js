/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.userBuildingSave')
    .component('userBuildingSave', {
        templateUrl: 'js/userBuildingSave/userBuildingSave.html',
        controller: UserBuildingSaveController,
        bindings: {
            pouchDb: '<',
            userBuildingLayer: '<'
        }
    });
UserBuildingSaveController.$inject = ['$scope', '$interval', '$uibModal', 'toastr', 'FileSaver', 'Blob'];
function UserBuildingSaveController($scope, $interval, $uibModal, toastr, FileSaver, Blob){
    var modalInstance;
    var changes;
    var ctrl = this;
    ctrl.name = "";
    ctrl.collections = [];
    ctrl.selectedCollection = getDefaultOptions();

    ctrl.$onDestroy = function(event){
        if(changes) changes.cancel();
    };

    ctrl.$onInit = function(){
        ctrl.loadUserCollections();
        listenChanges();
    };

    ctrl.open = function () {
        modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'userBuildingSaveModal.html',
            scope: $scope,
            size: "md"
        });
    };

    ctrl.cancel = function () {
        modalInstance.close();
    };

    ctrl.save = function(){
        if(_.isEqual(ctrl.selectedCollection.name, "-") && !_.isEmpty(ctrl.name)){
            ctrl.pouchDb.put({
                _id: ctrl.name,
                id: "" + _.now(),
                name: ctrl.name,
                features: getFeaturesObject()
            }).then(function (response) {
                // handle response
                ctrl.loadUserCollections();
                modalInstance.close();
            }).catch(function (err) {
                toastr.error(err.message);
            });
        }
        if(!_.isEqual(ctrl.selectedCollection.name, "-")){
            ctrl.pouchDb.get(ctrl.selectedCollection.name).then(function(doc) {
                return ctrl.pouchDb.put({
                    _id: ctrl.selectedCollection.name,
                    _rev: doc._rev,
                    id: "" + _.now(),
                    name: ctrl.selectedCollection.name,
                    features: getFeaturesObject()
                });
            }).then(function(response) {
                // handle response
                modalInstance.close();
            }).catch(function (err) {
                toastr.error(err.message);
            });
        }
    };

    ctrl.download = function(){
        var features = (new ol.format.GeoJSON()).writeFeatures(ctrl.userBuildingLayer.getSource().getFeatures(), {featureProjection: 'EPSG:3857'});
        var data = new Blob([features], { type: 'application/json;charset=utf-8' });
        FileSaver.saveAs(data, 'schwarzplan.json');
    };

    ctrl.loadUserCollections = function(){
        ctrl.pouchDb.allDocs({
            include_docs: true,
            attachments: false
        }).then(function (response) {
            // handle result
            var collections = [getDefaultOptions()].concat(_.map(response.rows, "doc"));
            updateCollections(collections);
        }).catch(function (err) {
            toastr.error(err.message);
        });
    };

    function listenChanges(){
        changes = ctrl.pouchDb.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', function(change) {
            // handle change
            ctrl.loadUserCollections();
        }).on('error', function (err) {
            toastr.error(err.message);
        });
    }

    function getFeaturesObject(){
        var features = ctrl.userBuildingLayer.getSource().getFeatures();
        return (new ol.format.GeoJSON()).writeFeatures(features, {featureProjection: 'EPSG:3857'});
    }

    function getDefaultOptions(){
        return {
            id: "none_" + _.now(),
            name: "-",
            features: []
        };
    }

    function updateCollections(collections){
        $interval(function(){
            ctrl.collections = collections;
        }, 1, 1);
    }
}