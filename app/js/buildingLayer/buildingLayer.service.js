/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.buildingLayer')
    .service('buildingLayer', buildingLayer);

buildingLayer.$inject = [];
function buildingLayer() {
    var buildingLayerAPI = {
        layerName: 'mcache',
        layer: null,
        getLayer: getLayer,
        addBuildingLayer: addBuildingLayer,
        setLayerVisibility: setLayerVisibility
    };

    return buildingLayerAPI;

    function getLayer(){
        return buildingLayerAPI.layer;
    }

    function addBuildingLayer(map, geoserver, wrkMap){
        if(_.isNull(geoserver) || _.isNull(wrkMap)) return;


        var wmsSourceP = new ol.source.ImageWMS({
            ratio: 1,
            url:  geoserver + '/geoserver/' + wrkMap+ '/wms',
            params: {
                'FORMAT': 'image/png8',
                'VERSION': '1.1.1',

                LAYERS: wrkMap + ':mcache_p',
                t:new Date().getMilliseconds()

            }
        });


        var wmsSourceT = new ol.source.TileWMS({
                /** Note use of workspace osm in the WMS url */
                url: geoserver + '/geoserver/' + wrkMap+ '/wms',
                projection: 'EPSG:900913',

                params: {
                    /** Workspace spec'ed above means we don't need it here */
                    'LAYERS': 'mcache',
                    /** PNG for street maps, JPG for aerials */
                    'FORMAT': 'image/png8',
                    'TILED': true
                },
                /** @type {ol.source.wms.ServerType} */
                serverType: 'geoserver'
            });


        var wmsSource = new ol.source.ImageWMS({
            ratio: 1,
            url: geoserver + '/geoserver/' + wrkMap+ '/wms',
            params: {
                'FORMAT': 'image/png',
                'VERSION': '1.1.1',

                LAYERS: wrkMap + ':mcache',
                t:new Date().getMilliseconds()
            }
        });

        buildingLayerAPI.layer = new ol.layer.Image({
            name: buildingLayerAPI.layerName,
            source: wmsSource,
            maxResolution: 3
        });
        //buildingLayerAPI.layer.setZIndex(100);
        map.addLayer(buildingLayerAPI.layer);



        map.addLayer(new ol.layer.Tile({
            name: buildingLayerAPI.layerName,
            source: wmsSourceT,
            minResolution: 4,
            maxResolution: 60
        }));



        map.addLayer(new ol.layer.Image({
            name: buildingLayerAPI.layerName,
            source: wmsSourceP,
            minResolution: 61
        }));
    }

    function setLayerVisibility(map, isVisible) {
        map.getLayers().forEach(function (layer) {
            if (buildingLayerAPI.layerName.includes(layer.get('name'))) {
                layer.setVisible(isVisible);
            }
        });
    }
}