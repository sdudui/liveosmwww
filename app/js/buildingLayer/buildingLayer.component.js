/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.buildingLayer')
    .component('buildingLayer', {
        templateUrl: 'js/buildingLayer/buildingLayer.html',
        controller: BuildingLayerController,
        bindings: {
            styleClass: '@',
            map: '<',
            geoserver: '<',
            wrkMap: '<'
        }
    });

BuildingLayerController.$inject = ['buildingLayer'];
function BuildingLayerController(buildingLayer) {
    var ctrl = this;
    var isLayerInitialized = false;
    ctrl.layerVisibility = true;
    ctrl.turnLayerVisibility = turnLayerVisibility;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isLayerInitialized){
            isLayerInitialized = true;
            buildingLayer.addBuildingLayer(ctrl.map, ctrl.geoserver, ctrl.wrkMap);
        }
    };

    function turnLayerVisibility() {
        ctrl.layerVisibility = !ctrl.layerVisibility;
        buildingLayer.setLayerVisibility(ctrl.map, ctrl.layerVisibility);
    }
}