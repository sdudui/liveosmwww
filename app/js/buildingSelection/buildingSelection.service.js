/**
 * Created by sm on 07.05.2017.
 */

angular.module('blackmap.buildingSelection')
    .service('buildingSelection', buildingSelection);

buildingSelection.$inject = ["$http"];
function buildingSelection($http) {
    var buildingSelectionAPI = {
        fetch: fetch
    };
    return buildingSelectionAPI;

    function fetch(geoserver, wrkMap, extent){
        var url = geoserver + "/geoserver/wfs";
        var bbox = extent.join(",") + ',EPSG:3857';
        var params = {
            request: "GetFeature",
            outputFormat: "application/json",
            mimeType: "application/json",
            srsname: 'EPSG:4326',
            typename: wrkMap + ":mcache",
            bbox: bbox
        };

        return $http.get(url, {params: params}).then(function (response) {
            return response.data;
        });
    }
}