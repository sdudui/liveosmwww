/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.buildingSelection')
    .component('buildingDragBoxSelection', {
        templateUrl: 'js/buildingSelection/buildingDragBoxSelection.html',
        controller: BuildingDragBoxSelectionController,
        bindings: {
            interactionLayers: '<',
            map: '=',
            geoserver: '<',
            wrkMap: '<',
            onBuildingSelected: '&'
        }
    });
BuildingDragBoxSelectionController.$inject = ['$interval', 'buildingSelection'];
function BuildingDragBoxSelectionController($interval, buildingSelection){
    var dragBoxInteraction;
    var ctrl = this;
    var isInteractionInitialized = false;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isInteractionInitialized){
            isInteractionInitialized = true;
            initBuildingDragBoxInteraction();
        }
    };

    function initBuildingDragBoxInteraction() {
        // a DragBox interaction used to select features by drawing boxes
        dragBoxInteraction = new ol.interaction.DragBox({
            layers: ctrl.interactionLayers,
            condition: ol.events.condition.platformModifierKeyOnly
        });

        ctrl.map.addInteraction(dragBoxInteraction);

        dragBoxInteraction.on('boxend', function() {
            var extent = dragBoxInteraction.getGeometry().getExtent();
            buildingSelection.fetch(ctrl.geoserver, ctrl.wrkMap, extent).then(function(response){
                var format = new ol.format.GeoJSON();
                var features = format.readFeatures(response, {featureProjection: 'EPSG:3857'});
                _.forEach(features, function(feature){
                    var featureExtent = feature.getGeometry().getExtent();
                    var topLeftPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getTopLeft(featureExtent));
                    var bottomRightPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getBottomRight(featureExtent));
                    var buildingToAdd = {
                        size: [Math.abs(topLeftPixel[0] - bottomRightPixel[0]), Math.abs(topLeftPixel[1] - bottomRightPixel[1])],
                        featureJson: format.writeFeatureObject(feature, {featureProjection: 'EPSG:3857'})
                    };
                    if(!_.isUndefined(ctrl.onBuildingSelected) && _.isFunction(ctrl.onBuildingSelected)){
                        $interval(function(){
                            ctrl.onBuildingSelected()(buildingToAdd);
                        }, 1, 1);
                    }
                });
            });
        });

        // clear selection when drawing a new box and when clicking on the map
        dragBoxInteraction.on('boxstart', function() {});
    }
}