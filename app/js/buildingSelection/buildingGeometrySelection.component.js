/**
 * Created by sm on 27.05.2017.
 */

angular
    .module('blackmap.buildingSelection')
    .component('buildingGeometrySelection', {
        templateUrl: 'js/buildingSelection/buildingGeometrySelection.html',
        controller: BuildingGeometrySelectionController,
        bindings: {
            isActive: '<',
            map: '=',
            geoserver: '<',
            wrkMap: '<',
            onBuildingSelected: '&',
            geometryInteraction: '<'
        }
    });
BuildingGeometrySelectionController.$inject = ['$interval', 'buildingSelection'];
function BuildingGeometrySelectionController($interval, buildingSelection) {
    var geometryInteraction = null;
    var source = null, vector = null, selectionsGeom = null;
    var style = new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(224, 115, 124, 0.2)'
        }),
        stroke: new ol.style.Stroke({
            color: '#e0737c',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: 'rgba(224, 115, 124, 0.8)'
            })
        })
    });
    var ctrl = this;
    var isInteractionInitialized = false;
    ctrl.selectionGeometries = [
        {
            id: "clear",
            name: "blackmap_buildingselection_clear_selection"
        },
        {
            id: "point",
            name: "blackmap_buildingselection_point_selection",
            olType: 'Point'
        },
        {
            id: "line",
            name: "blackmap_buildingselection_line_selection",
            olType: 'LineString'
        },
        {
            id: "rectangle",
            name: "blackmap_buildingselection_rectangle_selection",
            olType: 'Box'
        },
        {
            id: "circle",
            name: "blackmap_buildingselection_circle_selection",
            olType: 'Circle'
        },
        {
            id: "polygon",
            name: "blackmap_buildingselection_polygon_selection",
            olType: 'Polygon'
        }
    ];
    ctrl.toggleGeometrySelectionMode = toggleGeometrySelectionMode;

    ctrl.$onDestroy = function (event) {
        toggleGeometrySelectionMode();
    };
    ctrl.$onInit = function () {
    };
    ctrl.$doCheck = function () {
        if (!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isInteractionInitialized) {
            isInteractionInitialized = true;
            init();
        }

        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !_.isUndefined(geometryInteraction)){
            geometryInteraction.setActive(ctrl.isActive);
        }
    };

    function toggleGeometrySelectionMode(selected){
        if(!_.isNull(geometryInteraction)) {
            vector.getSource().clear();
            ctrl.map.removeInteraction(geometryInteraction);
            geometryInteraction.un('drawend', onDrawEnd);
            geometryInteraction.un('drawstart', onDrawStart);
        }

        if(_.isUndefined(selected)) return;
        if(_.isUndefined(selected.olType)) return;

        var type = selected.olType;
        var geometryFunction, maxPoints;
        if (_.isEqual(type, "Box")) {
            type = "LineString";
            maxPoints = 2;
            geometryFunction = function (coordinates, geometry) {
                if (!geometry) {
                    geometry = new ol.geom.Polygon(null);
                }
                var start = coordinates[0];
                var end = coordinates[1];
                geometry.setCoordinates([
                    [start, [start[0], end[1]], end, [end[0], start[1]], start]
                ]);
                return geometry;
            };
        }
        geometryInteraction = new ol.interaction.Draw({
            source: source,
            style: style,
            type: type,
            geometryFunction: geometryFunction,
            maxPoints: maxPoints
        });

        geometryInteraction.on('drawend', onDrawEnd);
        geometryInteraction.on('drawstart', onDrawStart);

        ctrl.map.addInteraction(geometryInteraction);
    }

    function init() {
        source = new ol.source.Vector({
            wrapX: false,
            projection: ctrl.map.getView().getProjection()
        });
        // http://openlayers.org/en/v3.8.2/examples/draw-features.html
        vector = new ol.layer.Vector({
            name: "Selections",
            source: source,
            style: style,
            map: ctrl.map
        });

        if(_.isUndefined(ctrl.geometryInteraction)) ctrl.geometryInteraction = "point";
        ctrl.selectionGeometry = _.find(ctrl.selectionGeometries, {id: ctrl.geometryInteraction});
        toggleGeometrySelectionMode(ctrl.selectionGeometry);
    }

    function onDrawEnd(event) {
        var extent = event.feature.getGeometry().getExtent();
        buildingSelection.fetch(ctrl.geoserver, ctrl.wrkMap, extent).then(function(response){
            var format = new ol.format.GeoJSON();
            var features = format.readFeatures(response, {featureProjection: 'EPSG:3857'});
            if(!_.isUndefined(ctrl.onBuildingSelected) && _.isFunction(ctrl.onBuildingSelected)){
                $interval(function(){
                    ctrl.onBuildingSelected()(_.map(features, function(feature){
                        var featureExtent = feature.getGeometry().getExtent();
                        var topLeftPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getTopLeft(featureExtent));
                        var bottomRightPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getBottomRight(featureExtent));
                        return {
                            //size: [Math.abs(topLeftPixel[0] - bottomRightPixel[0]), Math.abs(topLeftPixel[1] - bottomRightPixel[1])],
                            size: [100, 100],
                            featureJson: format.writeFeatureObject(feature, {featureProjection: 'EPSG:3857'})
                        };
                    }));
                }, 1, 1);
            }
        });
    }

    function onDrawStart() {
        vector.getSource().clear();
    }
}