/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.buildings')
    .component('buildings', {
        templateUrl: 'js/buildings/buildings.html',
        controller: BuildingsController,
        bindings: {
            dragBuilding: '=',
            dragBuildingTopLeftPixel: '=',
            features: '='
        }
    });
BuildingsController.$inject = [];
function BuildingsController(){
    var ctrl = this;
    ctrl.dragBuildingShape = dragBuildingShape;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    function dragBuildingShape(feature, event){
        if(feature){
            ctrl.dragBuilding = feature;
            ctrl.dragBuildingTopLeftPixel = $(event.currentTarget).position();
        }
        else{
            ctrl.dragBuilding = false;
            ctrl.dragBuildingTopLeftPixel = [0, 0];
        }
    }
}