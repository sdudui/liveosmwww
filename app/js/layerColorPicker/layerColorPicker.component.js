/**
 * Created by sm on 18.07.2017.
 */

angular
    .module('blackmap.layerColorPicker')
    .component('layerColorPicker', {
        templateUrl: 'js/layerColorPicker/layerColorPicker.html',
        controller: LayerColorPickerController,
        bindings: {
            layer: '<',
            color: '@',
            inputClass: '@'
        }
    });

LayerColorPickerController.$inject = [];
function LayerColorPickerController() {
    var ctrl = this;
    ctrl.colorOptions = {
        // html attributes
        id: "userBuildingColorPicker",
        required: false,
        disabled: false,
        placeholder: '',
        inputClass: "form-control",
        // color
        format: 'hexString',
        allowEmpty: false,
        restrictToFormat: false,
        hue: true,
        saturation: true,
        lightness: false, // Note: In the square mode this is HSV and in round mode this is HSL
        alpha: true,
        case: 'upper',
        // swatch
        swatch: true,
        swatchPos: 'left',
        swatchBootstrap: true,
        swatchOnly: true,
        // popup
        round: false,
        pos: 'top left',
        inline: false,
        // show/hide
        show: {
            swatch: true,
            focus: true
        },
        hide: {
            blur: true,
            escape: true,
            click: true
        },
        // buttons
        close: {
            show: false,
            label: 'Close',
            class: ''
        },
        clear: {
            show: false,
            label: 'Clear',
            class: ''
        },
        reset: {
            show: false,
            label: 'Reset',
            class: ''
        }
    };
    ctrl.colorEventApi = {
        onChange:  onColorChange.bind(this)
    };

    ctrl.$onDestroy = function(){};
    ctrl.$onInit = function(){
        ctrl.colorOptions.inputClass = ctrl.inputClass;
        onColorChange(null, ctrl.color, null);
    };

    function onColorChange(api, color, $event) {
        //Self defined style
        var newStyle= new ol.style.Style ({
            fill: new ol.style.Fill({
                color: color
            }),
            stroke: new ol.style.Stroke({
                color: "black",
                width: 2,
                lineCap: 'round'
            }),
            zIndex: 2
        });
        if(ctrl.layer) ctrl.layer.setStyle(newStyle);
        if(ctrl.colorApi) ctrl.colorApi.close();
    }
}