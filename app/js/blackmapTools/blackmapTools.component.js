/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.tools')
    .component('blackmapTools', {
        templateUrl: 'js/blackmapTools/blackmapTools.html',
        controller: BlackmapToolsController,
        bindings: {
            userBuildingLayer: '=',
            buildingLayer: '=',
            map: '=',
            geoserver: '<',
            wrkMap: '<',
            dragBuilding: '=',
            dragBuildingTopLeftPixel: '=',
            onAddSelectedBuildingFeatures: '&',
            maxBuildingSelections: '@'
        }
    });
BlackmapToolsController.$inject = ['$filter', '$interval', '$translate', 'toastr'];
function BlackmapToolsController($filter, $interval, $translate, toastr){
    var ctrl = this;
    ctrl.pouchDbName = "schwarzplan-userbuildings";
    ctrl.buildingLayers = [];
    ctrl.selectedBuildingFeatures = [];
    ctrl.isSelectMode = true;
    ctrl.loadFeatureFromCollection = loadFeatureFromCollection;
    ctrl.onBuildingSelected = onBuildingSelected;
    ctrl.clearSelections = clearSelections;
    ctrl.clearMap = clearMap;
    ctrl.toggleSelectionMode = toggleSelectionMode;

    ctrl.$onDestroy = function (event) {
        ctrl.pouchDb.close();
    };
    ctrl.$onInit = function () {
        // https://pouchdb.com/api.html#create_database
        ctrl.pouchDb = new PouchDB(ctrl.pouchDbName, {auto_compaction: true});
        ctrl.buildingLayers.push(ctrl.buildingLayer);
    };

    function loadFeatureFromCollection(features){
        var olFeatures = (new ol.format.GeoJSON()).readFeatures(features, {featureProjection: 'EPSG:3857'});
        loadFeaturesIntoSelectionsList(olFeatures);
        ctrl.userBuildingLayer.getSource().addFeatures(olFeatures);
        ctrl.map.getView().fit(ctrl.userBuildingLayer.getSource().getExtent(), ctrl.map.getSize());
    }

    function onBuildingSelected(buildingToAdd){
        ctrl.onAddSelectedBuildingFeatures()(true);
        var featuresCount = ctrl.selectedBuildingFeatures.length;
        var maxSelectionCount = _.isUndefined(ctrl.maxBuildingSelections) ? 50 : parseInt(ctrl.maxBuildingSelections);
        if(_.isArray(buildingToAdd)){
            featuresCount += buildingToAdd.length;
            ctrl.selectedBuildingFeatures = $filter("limitTo")(ctrl.selectedBuildingFeatures.concat(buildingToAdd).reverse(), maxSelectionCount);
        }
        else{
            featuresCount++;
            ctrl.selectedBuildingFeatures  = $filter("limitTo")(ctrl.selectedBuildingFeatures.push(buildingToAdd).reverse(), maxSelectionCount);
        }

        if(featuresCount > maxSelectionCount){
            var msg = $translate.instant("blackmap_blackmaptools_max_selection_reached");
            toastr.warning(msg.replace(/##replace##/gi, maxSelectionCount));
        }
    }

    function clearSelections(){
        ctrl.selectedBuildingFeatures = [];
    }

    function clearMap(){
        ctrl.userBuildingLayer.getSource().clear();
    }

    function toggleSelectionMode(){
        ctrl.isSelectMode = !ctrl.isSelectMode;
    }

    function loadFeaturesIntoSelectionsList(features){
        var format = new ol.format.GeoJSON();
        var selections = _.map(features, function(feature){
            var featureExtent = feature.getGeometry().getExtent();
            var topLeftPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getTopLeft(featureExtent));
            var bottomRightPixel = ctrl.map.getPixelFromCoordinate(ol.extent.getBottomRight(featureExtent));
            return {
                //size: [Math.abs(topLeftPixel[0] - bottomRightPixel[0]), Math.abs(topLeftPixel[1] - bottomRightPixel[1])],
                size: [100, 100],
                featureJson: format.writeFeatureObject(feature, {featureProjection: 'EPSG:3857'})
            };
        });

        ctrl.selectedBuildingFeatures = $filter("limitTo")(selections, parseInt(ctrl.maxBuildingSelections));
    }
}