/**
 * Created by sm on 07.05.2017.
 */

angular.module('blackmap.tools', [
    'toastr',
    'ui.toggle',
    'blackmap.userBuildingLoad',
    'blackmap.userBuildingSave',
    'blackmap.userBuildingRemove',
    'blackmap.userBuildingDownload',
    'blackmap.userBuildingUpload',
    'blackmap.buildings',
    'blackmap.buildingSelection',
    'blackmap.buildingTransform',
    'blackmap.layerColorPicker'
]);