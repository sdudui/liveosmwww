/**
 * Created by sm on 07.05.2017.
 */

angular.module('blackmap.wrkGridLayer')
    .service('wrkGridLayer', wrkGridLayer);

wrkGridLayer.$inject = [];
function wrkGridLayer() {
    var wrkGridLayerAPI = {
        layerName: 'wrkGrid',
        layer: null,
        getLayer: getLayer,
        addWrkGridLayer: addWrkGridLayer

    };

    return wrkGridLayerAPI;

    function getLayer() {
        return wrkGridLayerAPI.layer;
    }

    function addWrkGridLayer(map) {
        wrkGridLayerAPI.layer = new ol.layer.Vector({
            name: wrkGridLayerAPI.layerName,
            source: new ol.source.Vector(),
            style: function (feature) {

                var color = 'red';
                console.log(feature.get('status') );

                if (feature.status == "t")
                    color = 'blue';
                else if (feature.status == "p")
                    color = 'green';


                return new ol.style.Style(
                    {
                        stroke: new ol.style.Stroke(
                            {
                                color: color,
                                width: 2
                            }
                        )

                    }
                );

            }
        });
        wrkGridLayerAPI.layer.addFeatures = addFeatures;
        map.addLayer(wrkGridLayerAPI.layer);
    }





    function getOsmBuildings(cs, layer, s, $http, $timeout){

        $http({
            method: 'POST',
            url: "http://www.overpass-api.de/api/status",
            responseType: 'text',
            headers: {
                "Content-Type": "text/plain"
            }

        })
            .then(function successCallback(data) {


                if(data.data.indexOf("available now") !== -1 || data.data.indexOf("in 0 seconds")  !== -1){

                    $http({
                        method: 'POST',
                        url: "http://www.overpass-api.de/api/interpreter",
                        data: s.osmTileStub.query,
                        responseType: 'json',
                        headers: {
                            "Content-Type": "text/plain"
                        }

                    })
                        .then(function successCallback(data1) {
                            var src = data1;

                            s.status = 't';
                            s.osmTileStub.osmElements = data1.data;
                            s.tries = 2;
                            s.changed();
                            $timeout(function(){cs(cs, layer,  $http, $timeout)}, 500, false);
                        }, function errorCallback(response1) {
                            s.tries = s.tries + 1;
                            $timeout(function(){cs(cs, layer,  $http, $timeout)}, 500, false);
                        });

                }
                else{
                    var seconds = 2;
                    var i1 = data.data.indexOf("in ");
                    var i2 = data.data.indexOf(" seconds");
                    if(i1 !== -1 && i2 !== -1)
                        seconds = data.data.substring(i1 + 3 ,i2);
                    console.log(seconds);
                    layer.timeToNextCall = seconds;
                    $timeout(function(){cs(cs, layer,  $http, $timeout)}, seconds * 1000, false);
                }




            }, function errorCallback(response) {
                s.tries = s.tries + 1;
                $timeout(function(){cs(cs,layer,  $http, $timeout)}, 500, false);
            });



    }










    function addFeatures(osmTileStub,$http, $timeout){
        var layer = this;
        layer.getSource().clear(true);
        osmTileStub.forEach( function(s) {

            var format = new ol.format.WKT();
            var feature = format.readFeature(s.wkt);
            //feature.getGeometry().transform('EPSG:4326', 'EPSG:3857');
            feature.status = 'w';
            feature.tries = 0;
            feature.osmTileStub = s;
            layer.getSource().addFeature(feature);

        });


        var queryOverpass = function queryOverpass(cs, layer, $http, $timeout){
            var feats = layer.getSource().getFeatures();

            if(!layer.update)
                return;


            var found = false;
            feats.forEach(function(s){
                    if(s.status == 'w' && s.tries < 2 && !found) {

                        getOsmBuildings(cs, layer, s, $http, $timeout);
                        found = true;

                    }
                }

            );


        };


        (queryOverpass)(queryOverpass, layer,$http, $timeout);



    }


}