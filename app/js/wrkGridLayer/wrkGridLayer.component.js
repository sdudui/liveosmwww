/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.wrkGridLayer')
    .component('wrkGridLayer', {
        templateUrl: 'js/wrkGridLayer/wrkGridLayer.html',
        controller: WrkGridLayerController,
        bindings: {
            map: '<',
            wrkGridLayer: '='

        }
    });

WrkGridLayerController.$inject = ['wrkGridLayer'];
function WrkGridLayerController(wrkGridLayer) {
    var ctrl = this;
    var isLayerInitialized = false;
    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isLayerInitialized){
            isLayerInitialized = true;
            wrkGridLayer.addWrkGridLayer(ctrl.map);
            ctrl.wrkGridLayer = wrkGridLayer.getLayer();

        }
    };


}