/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.gridLayer')
    .component('gridLayer', {
        templateUrl: 'js/gridLayer/gridLayer.html',
        controller: GridLayerController,
        bindings: {
            map: '<',
            geoserver: '<',
            wrkMap: '<'
        }
    });

GridLayerController.$inject = ['gridLayer'];
function GridLayerController(gridLayer) {
    var ctrl = this;
    var isLayerInitialized = false;
    ctrl.layerVisibility = true;
    ctrl.turnLayerVisibility = turnLayerVisibility;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isLayerInitialized){
            isLayerInitialized = true;
            gridLayer.addGridLayer(ctrl.map, ctrl.geoserver, ctrl.wrkMap);
        }
    };

    function turnLayerVisibility() {
        ctrl.layerVisibility = !ctrl.layerVisibility;
        gridLayer.setLayerVisibility(ctrl.map, ctrl.layerVisibility);
    }
}