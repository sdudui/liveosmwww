/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.gridLayer')
    .service('gridLayer', gridLayer);

gridLayer.$inject = [];
function gridLayer() {
    var gridLayerAPI = {
        layerName: 'tcache',
        addGridLayer: addGridLayer,
        setLayerVisibility: setLayerVisibility
    };

    return gridLayerAPI;

    function addGridLayer(map, geoserver, wrkMap){
        if(_.isNull(geoserver) || _.isNull(wrkMap)) return;

        var wmsSourceT = new ol.source.ImageWMS({
            ratio: 1,
            url:  geoserver + '/geoserver/' + wrkMap+ '/wms',
            params: {
                'FORMAT': 'image/png',
                'VERSION': '1.1.1',

                LAYERS: wrkMap + ':tcache',
                t:new Date().getMilliseconds()
            }
        });

        map.addLayer(new ol.layer.Image({
            name: gridLayerAPI.layerName,
            source: wmsSourceT,
            maxResolution: 100
        }));
    }

    function setLayerVisibility(map, isVisible) {
        map.getLayers().forEach(function (layer) {
            if (gridLayerAPI.layerName.includes(layer.get('name'))) {
                layer.setVisible(isVisible);
            }
        });
    }
}