/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.userBuildingLoad')
    .component('userBuildingLoad', {
        templateUrl: 'js/userBuildingLoad/userBuildingLoad.html',
        controller: UserBuildingLoadController,
        bindings: {
            pouchDb: '<',
            onFeaturesLoad: '&?'
        }
    });
UserBuildingLoadController.$inject = ['$scope', '$interval', '$translate', '$uibModal', 'toastr'];
function UserBuildingLoadController($scope, $interval, $translate, $uibModal, toastr){
    var modalInstance;
    var changes;
    var defaultCollection = {
        id: "none_" + _.now(),
        name: "-",
        features: []
    };
    var ctrl = this;
    ctrl.uploadFile = null;
    ctrl.name = "";
    ctrl.collections = [];
    ctrl.selectedCollection = getDefaultOptions();

    ctrl.$onDestroy = function(event){
        if(changes) changes.cancel();
    };

    ctrl.$onInit = function(){
        ctrl.loadUserCollections();
        listenChanges();
    };

    ctrl.open = function () {
        modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'userBuildingLoadModal.html',
            scope: $scope,
            size: "md"
        });
    };

    ctrl.cancel = function () {
        modalInstance.close();
    };

    ctrl.load = function(){
        if(_.isUndefined(ctrl.onFeaturesLoad) || _.isNull(ctrl.onFeaturesLoad) || !_.isFunction(ctrl.onFeaturesLoad)) return;

        ctrl.onFeaturesLoad()(ctrl.selectedCollection.features);
        modalInstance.close();
    };

    ctrl.upload = function(){
        if(_.endsWith(ctrl.uploadFile.name, '.json') || _.endsWith(ctrl.uploadFile.name, '.geojson')){
            var reader = new FileReader();
            reader.onload = function () {
                var isValidData = true;
                var geojsonContent = reader.result;
                try{
                    var olFeatures = (new ol.format.GeoJSON()).readFeatures(geojsonContent, {featureProjection: 'EPSG:3857'});
                }catch(e){
                    isValidData = false;
                    toastr.error($translate.instant("blackmap_userbuildingupload_invalid_geojson_data"));
                }finally{
                    if(isValidData){
                        ctrl.onFeaturesLoad()(geojsonContent);
                        toastr.success($translate.instant("blackmap_userbuildingupload_success"));
                    }
                }
            };
            reader.onerror = function (event) {
                toastr.error($translate.instant("blackmap_userbuildingupload_error"));
            };
            reader.readAsText(ctrl.uploadFile);
            modalInstance.close();
        }
        else{
            toastr.error($translate.instant("blackmap_userbuildingupload_invalid_file_format"));
        }
    };

    ctrl.loadUserCollections = function(){
        ctrl.pouchDb.allDocs({
            include_docs: true,
            attachments: false
        }).then(function (response) {
            // handle result
            var collections = [getDefaultOptions()].concat(_.map(response.rows, "doc"));
            updateCollections(collections);
        }).catch(function (err) {
            toastr.error(err.message);
        });
    };

    function listenChanges(){
        changes = ctrl.pouchDb.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', function(change) {
            // handle change
            ctrl.loadUserCollections();
        }).on('error', function (err) {
            toastr.error(err.message);
        });
    }

    function getDefaultOptions(){
        return {
            id: "none_" + _.now(),
            name: "-",
            features: []
        };
    }

    function updateCollections(collections){
        $interval(function(){
            ctrl.collections = collections;
        }, 1, 1);
    }
}