/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.print', [
    'ngAnimate',
    'ngSanitize',

    // Third party angular components
    'ui.router',
    'ui.select',
    'ui.bootstrap.buttons',

    // Blackmap Modules
    'blackmap.map',
    'blackmap.autocomplete'
]);