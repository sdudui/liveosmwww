/**
 * Created by sm on 06.05.2017.
 */


angular.module('blackmap.print')
    .service('print', print);

print.$inject = ["$q","$http","userBuildingLayer"];
function print($q, $http, userBuildingLayer) {
    var printAPI = {
        getWH: getWH,
        getSldBody: getSldBody,
        printMap: printMap
    };
    return printAPI;

    function getWH(scale, format, position, dpi) {
        var a1 = 287 / 1000 * scale;
        var a2 = 200 / 1000 * scale;

        if (format == "a3")
            a2 = 410 / 1000 * scale;

        if (position == "l")
            return [Math.max(a1, a2), Math.min(a1, a2)];
        else
            return [Math.min(a1, a2), Math.max(a1, a2)];
    }


    function getDataUri(url,  data, callback) {
        var reader = new FileReader();

        $http({
            method: 'POST',
            url: url + '/GetMap',
            data: data,
            responseType: 'arraybuffer',
            config:{headers: {
                "Content-Type": "application/xml",
                "Accept": "image/png"
            }}
        })
            .then(function successCallback(imgData) {
                var  blob = new Blob([imgData.data], {type: 'image/png'});
                reader.readAsDataURL(blob);
            });

        reader.onload = function () {
            callback(reader.result);
        }

        reader.onerror = function(){
            callback(false);
        };


    }

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function constructSldBody(ubLayer){

        var frs = ubLayer.getSource().getFeatures();
        var style = ubLayer.getStyle();
        var fillColor = ol.color.asArray(style.getFill().getColor());
        var strokeColor = ol.color.asArray(style.getStroke().getColor());
        var strokeW = style.getStroke().getWidth();


        var sldBody;


        if(!(frs && frs.length > 0))
            sldBody = '<ogc:GetMap xmlns:ogc="http://www.opengis.net/ows" xmlns:gml="http://www.opengis.net/gml" version="1.0" service="WMS"><StyledLayerDescriptor version="1.0.0" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink"><sld:NamedLayer><sld:Name>osmsd:mcache</sld:Name> </sld:NamedLayer></StyledLayerDescriptor><BoundingBox srsName="http://www.opengis.net/gml/srs/epsg.xml#3857"><gml:coord decimal="."><gml:X>xmin</gml:X><gml:Y>ymin</gml:Y></gml:coord><gml:coord decimal="."><gml:X>xmax</gml:X><gml:Y>ymax</gml:Y></gml:coord></BoundingBox><Output><Format>image/png8</Format><Transparent>true</Transparent><BGcolor>#ffffff</BGcolor><Size><Width>width_px</Width><Height>height_px</Height></Size></Output><Exceptions>application/vnd.ogc.se+xml</Exceptions></ogc:GetMap>';
        else{
            sldBody = '<ogc:GetMap xmlns:ogc="http://www.opengis.net/ows" xmlns:gml="http://www.opengis.net/gml" version="1.0" service="WMS"><StyledLayerDescriptor version="1.0.0" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink"><sld:NamedLayer><sld:Name>osmsd:mcache</sld:Name> </sld:NamedLayer><sld:UserLayer><sld:Name>ImyB</sld:Name><sld:InlineFeature><sld:FeatureCollection>featurecollection</sld:FeatureCollection></sld:InlineFeature><sld:UserStyle><sld:FeatureTypeStyle><sld:Rule><sld:PolygonSymbolizer><Stroke><CssParameter name="stroke">strokeColor</CssParameter><CssParameter name="stroke-width">strokeW</CssParameter></Stroke><Fill><CssParameter name="fill">fillColor</CssParameter><CssParameter name="fill-opacity">fillOpacity</CssParameter></Fill></sld:PolygonSymbolizer></sld:Rule></sld:FeatureTypeStyle></sld:UserStyle></sld:UserLayer></StyledLayerDescriptor><BoundingBox srsName="http://www.opengis.net/gml/srs/epsg.xml#3857"><gml:coord decimal="."><gml:X>xmin</gml:X><gml:Y>ymin</gml:Y></gml:coord><gml:coord decimal="."><gml:X>xmax</gml:X><gml:Y>ymax</gml:Y></gml:coord></BoundingBox><Output><Format>image/png8</Format><Transparent>true</Transparent><BGcolor>#ffffff</BGcolor><Size><Width>width_px</Width><Height>height_px</Height></Size></Output><Exceptions>application/vnd.ogc.se+xml</Exceptions></ogc:GetMap>';

            sldBody = sldBody.replace("strokeW", strokeW);
            sldBody = sldBody.replace("fillColor", rgbToHex(fillColor[0],fillColor[1],fillColor[2] ));
            sldBody = sldBody.replace("strokeColor", rgbToHex(strokeColor[0],strokeColor[1], strokeColor[2] ));
            sldBody = sldBody.replace("fillOpacity", fillColor[3]);



            //var sldStart = '<StyledLayerDescriptor version="1.0.0" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" xmlns:xlink="http://www.w3.org/1999/xlink">';
            var gmlT = new ol.format.GML({featureNS:"osmsd",featureType:"MultiPolygon", srsName: 'EPSG:3857',multiSurface:false, surface:false,multiCurve:false});
            var sldStart = '<sld:featureMember><polygonProperty><MultiPolygon srsName="EPSG:3857"><polygonMember>';
            var sldend = '</polygonMember></MultiPolygon></polygonProperty></sld:featureMember>';
            var fs = gmlT.writeFeaturesNode(frs);
            var coords = fs.getElementsByTagName("posList");
            for(var i = 0; i < coords.length; i++){

                var clist = coords[i].innerHTML;
                var alist = clist.split(" ");
                var nclist = "";
                for(var j = 0; j < alist.length; j++){
                    nclist += alist[j] + "," + alist[j + 1];
                    j++;
                    if( j +1 < alist.length)
                        nclist += " ";
                }
                coords[i].innerHTML = nclist;
            }



            var geoms = fs.getElementsByTagName("Polygon");

            var gmls = "";
            for(var i = 0; i < geoms.length; i++){

                var gText =  geoms[i].outerHTML;
                gText = gText.replace(/exterior/g, "outerBoundaryIs");
                gText = gText.replace(/interior/g, "innerBoundaryIs");
                gText = gText.replace(/posList/g, "coordinates");

                gmls += sldStart + gText + sldend;
            }






            sldBody = sldBody.replace("featurecollection",gmls);

        }

        return  sldBody;

    }

    function getSldBody(map, wh, format, orientation, dpi){


        var ubLayer = userBuildingLayer.getLayer();

        var sldBody = constructSldBody(ubLayer);

        var mapCenter = map.getView().getCenter();
        var bbox = [
            mapCenter[0] - wh[0] / 2,
            mapCenter[1] - wh[1] / 2,
            mapCenter[0] + wh[0] / 2,
            mapCenter[1] + wh[1] / 2,
        ];

        var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(bbox),
            'EPSG:3857', 'EPSG:3857');
        var topRight = ol.proj.transform(ol.extent.getTopRight(bbox),
            'EPSG:3857', 'EPSG:3857');
        var params = {
            bbox: bottomLeft[0] + "," + bottomLeft[1] + "," + topRight[0] + "," + topRight[1]
        };


        sldBody = sldBody.replace('xmin',bottomLeft[0]);
        sldBody = sldBody.replace('ymin',bottomLeft[1]);

        sldBody = sldBody.replace('xmax', topRight[0]);
        sldBody = sldBody.replace('ymax', topRight[1]);

        var whp = [Math.floor(287 * 3508 / 297), Math.floor(200 * 2480 / 210)];
        if(format == "a3")
            whp = [Math.floor(287 * 3508 / 297), Math.floor(410 * 2480 / 210)];

        var doc = new jsPDF({
            orientation: orientation,
            unit: "mm",
            format: format
        });


        if (format == "a3")
            whp = [whp[0], whp[1] * 2]

        if(dpi == "düi600")
            whp = [whp[0] * 2, whp[1] * 2]


        if (orientation == "l") {
            params.width = Math.max(whp[0], whp[1]);
            params.height = Math.min(whp[0], whp[1]);
        }
        else {
            params.width = Math.min(whp[0], whp[1]);
            params.height = Math.max(whp[0], whp[1]);

        }


        sldBody = sldBody.replace('width_px', params.width);
        sldBody = sldBody.replace('height_px', params.height);

        return sldBody;
    }

// Usage
//now call the generateImagePdf function, which will call the generateBase64Image function and wait for tyhe image to load to call the generatePdf function with the param
    function printMap(url, sldBody, format, orientation) {
        var defer = $q.defer();
        getDataUri(url, sldBody, function (dataUri) {
            if(!dataUri){
                defer.reject(true);
            }
            else{
                var doc = new jsPDF({
                    orientation: orientation,
                    unit: "mm",
                    format: format
                });
                var wh = [200, 287];
                if (format == "a3")
                    wh = [410, 287];try {
                    if (orientation == "l")
                        doc.addImage(dataUri, 'PNG', 5, 5, Math.max(wh[0], wh[1]), Math.min(wh[0], wh[1]));
                    else
                        doc.addImage(dataUri, 'PNG', 5, 5, Math.min(wh[0], wh[1]), Math.max(wh[0], wh[1]));
                }
                catch(e) {
                    //do nothing, for the case the image is white
                }
                doc.setTextColor(255, 0, 0);
                doc.textWithLink('der-geograph.de', 10, 10, {url: 'http://der-geograph.de'});
                doc.save("blackmap.pdf");
                defer.resolve(true);
            }
        });


        return defer.promise;
    }
}