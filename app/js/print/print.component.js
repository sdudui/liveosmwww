/**
 * Created by sm on 06.05.2017.
 */
angular
    .module('blackmap.print')
    .component('print', {
        templateUrl: 'js/print/print.html',
        controller: PrintController,
        bindings: {
            map: '<',
            geoserver: '<',
            wrkMap: '<',
            defaultLocation: '<?',
            defaultScale: '<?',
            defaultFormat: '<?',
            defaultOrientation: '<?',
            defaultDpi: '<?',
            printNow: '<?'
        }
    });

PrintController.$inject = ['toastr', '$translate', '$state', 'print', 'olmap'];
function PrintController(toastr, $translate, $state, print, olmap) {
    var wh;
    var vectorContext;
    var ctrl = this;
    ctrl.isPrintDisable = true;
    ctrl.radioModelF = 'a4';
    ctrl.radioModelP = 'p';
    ctrl.radioModelDPI = 'dpi300';
    ctrl.scale = {
        selected: {
            key: 1000,
            value: '1:1.000'
        },
        options: [
            {
                key: 500,
                value: '1:500'
            },
            {
                key: 1000,
                value: '1:1.000'
            },
            {
                key: 2500,
                value: '1:2.500'
            },
            {
                key: 5000,
                value: '1:5.000'
            },
            {
                key: 10000,
                value: '1:10.000'
            },
            {
                key: 25000,
                value: '1:25.000'
            },
            {
                key: 50000,
                value: '1:50.000'
            },
            {
                key: 100000,
                value: '1:100.000'
            }
        ]
    };
    ctrl.printState = {};
    ctrl.printState.disabled = false;
    ctrl.printState.classf = function (v) {
        return function (v) {
            ctrl.printState.class = v;
            ctrl.printState.disabled = (v != "");
        }
    }("");

    ctrl.$onDestroy = function (event) {
        vectorContext = null;
        ctrl.map.un('postcompose', mapPostCompose);
        ctrl.map.render();
    };
    ctrl.$onInit = function () {
        ctrl.map = olmap.getOlMap();
        ctrl.geoserver = olmap.getGeoserverUrl();
        ctrl.wrkMap = olmap.getWrkMap();
        if(ctrl.defaultLocation){
            var coords = ol.proj.transform([ctrl.defaultLocation.id.lng, ctrl.defaultLocation.id.lat], "EPSG:4326", "EPSG:3857");
            ctrl.map.getView().setCenter(coords);
        }
        if(ctrl.defaultScale){
            var foundScale = _.find(ctrl.scale.options, {key: ctrl.defaultScale});
            ctrl.scale.selected = _.isUndefined(foundScale) ? ctrl.scale.selected : foundScale;
        }
        if(ctrl.defaultFormat) ctrl.radioModelF = ctrl.defaultFormat;
        if(ctrl.defaultOrientation) ctrl.radioModelP = ctrl.defaultOrientation;
        if(ctrl.defaultDpi) ctrl.radioModelDPI = ctrl.defaultDpi;
        ctrl.map.on('postcompose', mapPostCompose);
        ctrl.setPreview();
        if(ctrl.printNow) ctrl.printMap();
    };
    ctrl.setPreview = function () {
        updateClientUIStateInUrl();
        ctrl.isPrintDisable = false;
        wh = print.getWH(ctrl.scale.selected.key, ctrl.radioModelF, ctrl.radioModelP, ctrl.radioModelDPI);
        ctrl.map.render();
    };
    ctrl.printMap = function () {
        ctrl.printState.classf("glyphicon glyphicon-refresh glyphicon-refresh-animate");
        var sldBody = print.getSldBody(ctrl.map, wh,  ctrl.radioModelF, ctrl.radioModelP, ctrl.radioModelDPI);
        print.printMap(ctrl.geoserver + "/geoserver/wms", sldBody, ctrl.radioModelF, ctrl.radioModelP).then(function(){
            ctrl.printState.classf("");
        }, function(){
            ctrl.printState.classf("");
            toastr.error($translate.instant("blackmap_print_failed_title"));
        });
    };

    function mapPostCompose(evt) {
        if (wh) {
            var mapCenter = ctrl.map.getView().getCenter();
            vectorContext = evt.vectorContext;
            var printRectangle = new ol.geom.Polygon.fromExtent([
                mapCenter[0] - wh[0] / 2,
                mapCenter[1] - wh[1] / 2,
                mapCenter[0] + wh[0] / 2,
                mapCenter[1] + wh[1] / 2
            ]);
            var fill = new ol.style.Fill({color: [255, 255, 0, 0.3]});
            var stroke = new ol.style.Stroke({color: [55, 255, 0, 0.8]});
            var style = new ol.style.Style({
                fill: fill,
                stroke: stroke,
                image: new ol.style.Circle({
                    radius: 10,
                    fill: fill,
                    stroke: stroke
                })
            });
            vectorContext.setStyle(style);
            vectorContext.drawGeometry(printRectangle);
        }
    }

    /**
     * Reflect client UI state in the url without reloading view.
     */
    function updateClientUIStateInUrl(){
        var stateParams = {
            scale: ctrl.scale.selected.key,
            format: ctrl.radioModelF,
            orientation: ctrl.radioModelP,
            dpi: ctrl.radioModelDPI
        };
        $state.go($state.$current.self.name, stateParams, {reload: false, notify: false});
    }
}