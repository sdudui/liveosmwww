/**
 * Created by sm on 13.05.2017.
 */
(function(){
    angular.module('blackmap.print')
        .config(configure);
    configure.$inject = ['$stateProvider'];
    function configure($stateProvider) {

        // Routes config
        var states = [
            {
                name: 'blackmap.print',
                url: '/print?location&scale&format&orientation&dpi&printNow',
                component: 'print',
                resolve: {
                    defaultLocation: ['$stateParams', 'autocomplete', function($stateParams, autocomplete){
                        return autocomplete.fetchLocations($stateParams.location).then(function(response){
                            return response[0];
                        });
                    }],
                    defaultScale: ['$stateParams', function($stateParams){
                        return parseInt($stateParams.scale);
                    }],
                    defaultFormat: ['$stateParams', function($stateParams){
                        return $stateParams.format;
                    }],
                    defaultOrientation: ['$stateParams', function($stateParams){
                        return $stateParams.orientation;
                    }],
                    defaultDpi: ['$stateParams', function($stateParams){
                        return $stateParams.dpi;
                    }],
                    printNow: ['$stateParams', function($stateParams){
                        return !_.isUndefined($stateParams.printNow);
                    }]
                }
            }
        ];
        // Loop over the state definitions and register them
        states.forEach(function(state) {
            $stateProvider.state(state);
        });
    }
}());
