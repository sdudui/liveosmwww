/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.buildingTransform')
    .component('buildingTransform', {
        templateUrl: 'js/buildingTransform/buildingTransform.html',
        controller: BuildingTransformController,
        bindings: {
            interactionLayer: '<',
            map: '<',
            isActive: '<'
        }
    });
BuildingTransformController.$inject = [];
function BuildingTransformController(){
    var isActive = false;
    var ctrl = this;
    var isInteractionInitialized = false;
    ctrl.transformInteraction = null;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !_.isNull(ctrl.interactionLayer) && !_.isUndefined(ctrl.interactionLayer) && !isInteractionInitialized){
            isInteractionInitialized = true;
            initBuildingTransformInteraction();
        }

        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !_.isUndefined(ctrl.transformInteraction) && isInteractionInitialized
            && !_.isEqual(isActive, ctrl.isActive)){
            isActive = ctrl.isActive;
            ctrl.transformInteraction.setActive(ctrl.isActive);
        }
    };

    function initBuildingTransformInteraction() {
        // Scale and rotate the feature
        // http://viglino.github.io/ol3-ext/examples/map.interaction.transform.html
        // http://openlayers.org/en/latest/examples/select-features.html
        // https://openlayers.org/en/latest/examples/custom-interactions.html
        ctrl.transformInteraction = new ol.interaction.Transform({
            condition: function(mapBrowserEvent) {
                return ol.events.condition.noModifierKeys;
            },
            layers: [ctrl.interactionLayer],
            translateFeature: true,
            scale: true,
            rotate: true,
            keepAspectRatio: ol.events.condition.always,
            translate: true,
            stretch: false
        });
        ctrl.map.addInteraction(ctrl.transformInteraction);
    }
}