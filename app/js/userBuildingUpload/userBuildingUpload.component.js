/**
 * Created by sm on 14.05.2017.
 */

angular
    .module('blackmap.userBuildingUpload')
    .component('userBuildingUpload', {
        templateUrl: 'js/userBuildingUpload/userBuildingUpload.html',
        controller: UserBuildingUploadController,
        bindings: {
            onFeaturesUpload: '&'
        }
    });
UserBuildingUploadController.$inject = ['$translate', 'toastr'];
function UserBuildingUploadController($translate, toastr) {
    var ctrl = this;
    ctrl.uploadFile = null;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};

    ctrl.upload = function(){
        if(_.endsWith(ctrl.uploadFile.name, '.json') || _.endsWith(ctrl.uploadFile.name, '.geojson')){
            var reader = new FileReader();
            reader.onload = function () {
                var isValidData = true;
                var geojsonContent = reader.result;
                try{
                    var olFeatures = (new ol.format.GeoJSON()).readFeatures(geojsonContent, {featureProjection: 'EPSG:3857'});
                }catch(e){
                    isValidData = false;
                    toastr.error($translate.instant("blackmap_userbuildingupload_invalid_geojson_data"));
                }finally{
                    if(isValidData){
                        ctrl.onFeaturesUpload()(geojsonContent);
                        toastr.success($translate.instant("blackmap_userbuildingupload_success"));
                    }
                }
            };
            reader.onerror = function (event) {
                toastr.error($translate.instant("blackmap_userbuildingupload_error"));
            };
            reader.readAsText(ctrl.uploadFile);
        }
        else{
            toastr.error($translate.instant("blackmap_userbuildingupload_invalid_file_format"));
        }
    };
}