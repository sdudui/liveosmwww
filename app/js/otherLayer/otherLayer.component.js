/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.otherLayer')
    .component('otherLayer', {
        templateUrl: 'js/otherLayer/otherLayer.html',
        controller: OtherLayerController,
        bindings: {
            styleClass: '@',
            map: '<',
            geoserver: '<',
            wrkMap: '<',
            layerName: '@',
            layerGeoserverName: '@',
            maxResolution: '@'

    }
    });

OtherLayerController.$inject = ['otherLayer'];
function OtherLayerController(otherLayer) {
    var ctrl = this;
    var isLayerInitialized = false;
    ctrl.layerVisibility = false;
    ctrl.turnLayerVisibility = turnLayerVisibility;
    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isLayerInitialized){
            isLayerInitialized = true;
            otherLayer.addOtherLayer(ctrl.map, ctrl.geoserver, ctrl.wrkMap, ctrl.layerGeoserverName, ctrl.maxResolution );
        }
    };

    function turnLayerVisibility() {
        ctrl.layerVisibility = !ctrl.layerVisibility;
        otherLayer.setLayerVisibility(ctrl.map, ctrl.layerVisibility,ctrl.layerGeoserverName);
    }
}