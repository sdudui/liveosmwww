/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.otherLayer')
    .service('otherLayer', otherLayer);

otherLayer.$inject = [];
function otherLayer() {
    var otherLayerAPI = {
        addOtherLayer: addOtherLayer,
        setLayerVisibility: setLayerVisibility
    };

    return otherLayerAPI;



    function addOtherLayer(map, geoserver, wrkMap, layerGeoServerName,maxResolution){
        otherLayerAPI.layerName = layerGeoServerName;
        if(_.isNull(geoserver) || _.isNull(wrkMap)) return;




        var wmsSourceT = new ol.source.TileWMS({
            /** Note use of workspace osm in the WMS url */
            url: geoserver + '/geoserver/' + wrkMap+ '/wms',
            projection: 'EPSG:900913',

            params: {
                /** Workspace spec'ed above means we don't need it here */
                'LAYERS': layerGeoServerName,
                /** PNG for street maps, JPG for aerials */
                'FORMAT': 'image/png8',
                'TILED': true
            },
            /** @type {ol.source.wms.ServerType} */
            serverType: 'geoserver'
        });





        map.addLayer(new ol.layer.Tile({
            name: otherLayerAPI.layerName,
            source: wmsSourceT,
            visible:false,
            maxResolution:maxResolution


        }));



    }

    function setLayerVisibility(map, isVisible,layerName) {
        map.getLayers().forEach(function (layer) {
            if (layerName.includes(layer.get('name'))) {
                layer.setVisible(isVisible);
            }
        });
    }
}