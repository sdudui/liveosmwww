/**
 * Created by sm on 06.05.2017.
 */

angular.module('blackmap.map')
    .service('olmap', map);

map.$inject = ['$http'];
function map($http) {
    var geoserver;
    var wrkMap;
    var view = new ol.View({
        zoom: 17
    });

    var mapAPI = {
        olMap: null,
        mapOptions: {
            target: 'map',
            controls: ol.control.defaults({
                zoom: true,
                attribution: false,
                rotate: false
            }).extend([
                new ol.control.ScaleLine()
            ]),
            layers: [new ol.layer.Tile({
                name: "osm",
                source: new ol.source.OSM(),
                attributions: []
            })],
            // Improve user experience by loading tiles while animating. Will make
            // animations stutter on mobile or slow devices.
            loadTilesWhileAnimating: true,
            view: view
        },
        getOlMap: getOlMap,
        getGeoserverUrl: getGeoserverUrl,
        getWrkMap: getWrkMap,
        getUrls: getUrls
    };

    return mapAPI;

    function getOlMap(){
        if(_.isNull(mapAPI.olMap)){
            mapAPI.olMap = new ol.Map(mapAPI.mapOptions);
        }
        return mapAPI.olMap;
    }

    function getGeoserverUrl(){
        return geoserver;
    }

    function getWrkMap(){
        return wrkMap;
    }

    function getUrls(ajaxServer) {
        return $http({
            method: 'GET',
            url: ajaxServer + '/mapurl'
        }).then(function (response) {
            var data = response.data;
            geoserver = data.value0[0];
            wrkMap = data.value0[1];
        });
    }
}