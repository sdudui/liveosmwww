/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.map')
    .component('blackmapMap', {
        templateUrl: 'js/map/map.html',
        controller: MapController,
        bindings: {
            map: '=',
            geoserver: '=',
            wrkMap: '='
        }
    });

MapController.$inject = ['$scope', '$interval', '$window', 'olmap'];
function MapController($scope, $interval, $window, map) {
    var ctrl = this;
    var romeExtent = function (ext4326) {
        var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(ext4326),
            'EPSG:4326', 'EPSG:3857');
        var topRight = ol.proj.transform(ol.extent.getTopRight(ext4326),
            'EPSG:4326', 'EPSG:3857');
        return [bottomLeft[0], bottomLeft[1], topRight[0], topRight[1]];

    }([-5, 36, 13, 54]);
    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {
        ctrl.map = map.getOlMap();
        ctrl.map.getView().fit(romeExtent, ctrl.map.getSize());
        ctrl.geoserver = map.getGeoserverUrl();
        ctrl.wrkMap = map.getWrkMap();
    };
    ctrl.$postLink = function () {
        ctrl.map.on('moveend', function (evt) {
            var imap = evt.map;
            var args = [];
            args.push(imap.getView().getResolution());
            args.push(imap.getView().getZoom());
            $scope.$apply(null, args);
        });
        $window.onresize = onWindowResize;
    };

    function onWindowResize() {
        $interval(
            function () {
                ctrl.map.updateSize();
            }, 100, 1);
    }
}