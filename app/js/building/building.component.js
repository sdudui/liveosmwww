/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.building')
    .component('building', {
        templateUrl: 'js/building/building.html',
        controller: BuildingController,
        bindings: {
            feature: '<',
            onSelect: '&'
        }
    });
BuildingController.$inject = ['$element', '$attrs', '$interval'];
function BuildingController($element, $attrs, $interval){
    var svg = null;
    var ctrl = this;
    ctrl.mouseEventFlag = "out";

    ctrl.$onDestroy = function(event){
        svg = null;
        $element.empty();
    };

    ctrl.$onInit = function(){
        svg = $element.find("svg")[0];

        //Define map projection
        var projection = d3.geoMercator()
            .fitSize(ctrl.feature.size, ctrl.feature.featureJson);

        //Define path generator
        var path = d3.geoPath()
            .projection(projection);

        //Bind data and create one path per GeoJSON feature
        d3.select(svg)
            .datum(ctrl.feature.featureJson)
            .append("path")
            .attr("class", "buildingPath")
            .attr("d", path);
    };

    ctrl.select = function(event){
        if(_.isUndefined($attrs.draggableBuilding) && _.isEqual(ctrl.mouseEventFlag, "out")) {
            ctrl.mouseEventFlag = "over";
            $interval(function(){
                ctrl.onSelect()(false, event);
                $interval(function(){
                    ctrl.onSelect()(ctrl.feature, event);
                },1,1);
            },1,1);
        }
    };
    ctrl.unselect = function(event){
        ctrl.mouseEventFlag = "out";
    }
}