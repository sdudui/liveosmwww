/**
 * Created by sm on 12.05.2017.
 */

angular
    .module('blackmap.userBuildingRemove')
    .component('userBuildingRemove', {
        templateUrl: 'js/userBuildingRemove/userBuildingRemove.html',
        controller: UserBuildingRemoveController,
        bindings: {
            pouchDb: '<',
            userBuildingLayer: '<'
        }
    });
UserBuildingRemoveController.$inject = ['$scope', '$interval', '$q', '$uibModal', '$translate', 'toastr'];
function UserBuildingRemoveController($scope, $interval, $q, $uibModal, $translate, toastr) {
    var modalInstance;
    var changes;
    var ctrl = this;
    ctrl.name = "";
    ctrl.collections = [];
    ctrl.selectedCollection = getDefaultOptions()[0];

    ctrl.$onDestroy = function (event) {
        if (changes) changes.cancel();
    };

    ctrl.$onInit = function () {
        ctrl.loadUserCollections();
        listenChanges();
    };

    ctrl.open = function () {
        modalInstance = $uibModal.open({
            animation: true,
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'userBuildingRemoveModal.html',
            scope: $scope,
            size: "md"
        });
    };

    ctrl.cancel = function () {
        modalInstance.close();
    };

    ctrl.remove = function () {
        modalInstance.close();
        if(_.isEqual(ctrl.selectedCollection.name,  "-")) return;

        if (_.isUndefined(ctrl.selectedCollection.isAll)) {
            ctrl.pouchDb.get(ctrl.selectedCollection.name)
                .then(function (doc) {
                    toastr.success(ctrl.selectedCollection.name + " " + $translate.instant("blackmap_userbuildingremove_remove_document_success"));
                    return ctrl.pouchDb.remove(doc);
                })
                .catch(function (err) {
                    toastr.error(err.message);
                });
        }
        else {
            ctrl.pouchDb.allDocs().then(function (result) {
                return $q.all(result.rows.map(function (row) {
                    return ctrl.pouchDb.remove(row.id, row.value.rev);
                }));
            }).then(function () {
                toastr.success($translate.instant("blackmap_userbuildingremove_remove_all_success"));
            }).catch(function (err) {
                toastr.error(err.message);
            });
        }
    };

    ctrl.loadUserCollections = function () {
        ctrl.pouchDb.allDocs({
            include_docs: true,
            attachments: false
        }).then(function (response) {
            // handle result
            var collections = getDefaultOptions().concat(_.map(response.rows, "doc"));
            updateCollections(collections);
        }).catch(function (err) {
            toastr.error(err.message);
        });
    };

    function listenChanges() {
        changes = ctrl.pouchDb.changes({
            since: 'now',
            live: true,
            include_docs: true
        }).on('change', function (change) {
            // handle change
            ctrl.loadUserCollections();
        }).on('error', function (err) {
            toastr.error(err.message);
        });
    }

    function getDefaultOptions() {
        return [
            {
                id: "none_" + _.now(),
                name: "-",
                features: []
            },
            {
                id: "all_" + _.now(),
                name: "blackmap_userbuildingremove_remove_all",
                isAll: true,
                features: []
            }
        ];
    }

    function updateCollections(collections) {
        $interval(function () {
            ctrl.collections = collections;
        }, 1, 1);
    }
}