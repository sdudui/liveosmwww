/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.geolocation')
    .component('geolocation', {
        templateUrl: 'js/geolocation/geolocation.html',
        controller: GeolocationController,
        bindings: {
            map: '<',
            isAppStartDoGeolocation: '<',
            onGeolocation: '&'
        }
    });

GeolocationController.$inject = ['$location', 'toastr'];
function GeolocationController($location, toastr) {
    var ctrl = this;
    var isGeolocationInitialized = false;
    ctrl.isSecureProtocol = _.isEqual($location.protocol(), "https");
    ctrl.geolocation = null;
    ctrl.moveToGelocation = moveToGelocation;

    ctrl.$onDestroy = function (event) {};
    ctrl.$onInit = function () {};
    ctrl.$doCheck = function () {
        if(!_.isNull(ctrl.map) && !_.isUndefined(ctrl.map) && !isGeolocationInitialized && ctrl.isSecureProtocol ){
            isGeolocationInitialized = true;
            doGeolocation();
        }
    };

    function moveToGelocation(){
        ctrl.onGeolocation()(ctrl.geolocation.getPosition(), true, false);
    }

    function doGeolocation() {
        ctrl.geolocation = new ol.Geolocation({
            projection: ctrl.map.getView().getProjection(),
            tracking: true
        });
        ctrl.geolocation.on('error', function(error){
            toastr.error(error.message);
        });
        if(ctrl.isAppStartDoGeolocation){
            ctrl.geolocation.once('change', function(){
                ctrl.onGeolocation()(ctrl.geolocation.getPosition(), true, false);
            });
        }
    }
}