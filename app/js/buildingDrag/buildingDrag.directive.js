/**
 * Created by sm on 07.05.2017.
 */

angular
    .module('blackmap.buildingDrag')
    .directive('draggableBuilding', draggableBuilding);

draggableBuilding.$inject = ['$document'];
function draggableBuilding($document) {
    return function(scope, element, attr) {
        var featureTopLeftPixel = JSON.parse(attr.draggableBuilding);
        var startX = 0, startY = 0, x = featureTopLeftPixel.left, y = featureTopLeftPixel.top;
        element.css({
            position: 'absolute',
            top: featureTopLeftPixel.top + 'px',
            left: featureTopLeftPixel.left + 'px',
            zIndex: 1,
            border: 'none',
            backgroundColor: 'none',
            cursor: 'move',
            display: 'block',
            width: '65px'
        });

        element.on('mousedown', function(event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            event.stopImmediatePropagation();
            startX = event.clientX - x;
            startY = event.clientY - y;
            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);
        });

        function mousemove(event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            y = event.clientY - startY;
            x = event.clientX - startX;
            element.css({
                top: y + 'px',
                left:  x + 'px'
            });
        }

        function mouseup(event) {
            event.preventDefault();
            event.stopImmediatePropagation();
            $document.off('mousemove', mousemove);
            $document.off('mouseup', mouseup);
            //scope.$emit("SHAPE_DRAG_END", event);
            //console.log(scope.$parent);
            //console.log(event);
            if(scope.$parent) scope.$parent.$ctrl.dragEndBuilding(event);
        }
    };
}