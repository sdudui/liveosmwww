/**
 * Created by sm on 06.05.2017.
 */

angular
    .module('blackmap.featureExport')
    .component('featureExport', {
        templateUrl: 'js/featureExport/featureExport.html',
        controller: FeatureExportController,
        bindings: {
            map: '<',
            geoserver: '<',
            wrkMap: '<'
        }
    });

FeatureExportController.$inject = ['featureExport'];
function FeatureExportController(featureExport) {
    var ctrl = this;
    ctrl.exportOptions = ['png', 'svg', 'kmz', 'kml', 'json', 'shp'];
    ctrl.download = download;

    ctrl.$onDestroy = function (event) {
    };

    ctrl.$onInit = function () {
    };

    function download(type) {
        featureExport.exportAs(ctrl.map, ctrl.geoserver, ctrl.wrkMap, type);
    }
}