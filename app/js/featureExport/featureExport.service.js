/**
 * Created by sm on 06.05.2017.
 */


angular.module('blackmap.featureExport')
    .service('featureExport', featureExport);

featureExport.$inject = ["$http"];
function featureExport($http) {
    var featureExportAPI = {
        exportAs: exportAs
    };
    return featureExportAPI;

    function exportAs(map, geoserver, wrkMap, type) {
        var zoom = map.getView().getZoom();
        var mimeType;
        if (zoom < 12) {
            map.getView().setZoom(13);
        }

        //var url = geoserver + "/geoserver/osm.sd/wms";
        var url = geoserver + "/geoserver/wms";
        var bbox = map.getView().calculateExtent(map.getSize());
        var bottomLeft = ol.proj.transform(ol.extent.getBottomLeft(bbox),
            'EPSG:3857', 'EPSG:3857');
        var topRight = ol.proj.transform(ol.extent.getTopRight(bbox),
            'EPSG:3857', 'EPSG:3857');
        var params = {
            bbox: bottomLeft[0] + "," + bottomLeft[1] + "," + topRight[0] + "," + topRight[1]
        };

        if (type == "png") {
            params.request = "GetMap";
            params.format = "image/" + type;
            mimeType = params.format;
            params.srs = 'EPSG:3857';
            params.service = 'WMS';
            params.layers = wrkMap + ':mcache';
            params.width = Math.floor(120 / 90 * map.getSize()[0]);
            params.height = Math.floor(120 / 90 * map.getSize()[1]);
        }
        if (type == "svg") {
            params.request = "GetMap";
            params.format = "image/" + type;
            mimeType = params.format;
            params.srs = 'EPSG:3857';
            params.service = 'WMS';
            params.layers = wrkMap + ':mcache';
            params.width = map.getSize()[0];
            params.height = map.getSize()[1];
        }
        if (type == "kmz" || type == "kml") {
            params.request = "GetMap";
            params.format = type;
            if (type == "kmz")
                mimeType = "application/vnd.google-earth.kmz";
            else
                mimeType = "application/vnd.google-earth.kml+xml";
            params.srs = 'EPSG:3857';
            params.service = 'WMS';
            params.layers = wrkMap + ':mcache';
            params.width = map.getSize()[0];
            params.height = map.getSize()[1];
        }
        if (type == "shp") {
            url = geoserver + "/geoserver/wfs";
            params.request = "GetFeature";
            type = 'zip';
            params.outputFormat = "shape-zip";
            mimeType = params.outputFormat;
            params.srsname = 'EPSG:4326';
            params.typename = wrkMap + ":mcache";
            params.bbox = params.bbox + ',EPSG:3857';
        }
        if (type == "json") {
            url = geoserver + "/geoserver/wfs";
            params.request = "GetFeature";
            params.outputFormat = "application/json";
            mimeType = params.outputFormat;
            params.srsname = 'EPSG:4326';
            params.typename = wrkMap + ":mcache";
            params.bbox = params.bbox + ',EPSG:3857';
        }

        $http.get(url, {
            params: params,
            responseType: "arraybuffer"
        }).then(function (response) {
            var data = response.data;
            var anchor = angular.element('<a/>');
            var blob = new Blob([data], {type: mimeType});
            angular.element(document.body).append(anchor);
            anchor.attr({
                href: window.URL.createObjectURL(blob),
                target: '_blank',
                download: 'blackmap.' + type
            })[0].click();

        })
    }
}